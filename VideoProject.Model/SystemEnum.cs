﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Model
{
    public static class SystemEnum
    {
        public enum RedirectVideoType
        {
            // DO NOT CHANGE HERE WITHOUT PERMISSION
            Link = 1,
            Contact = 2,
            Skype = 3
            // DO NOT CHANGE HERE WITHOUT PERMISSION
        };

        public enum UserRole
        {
            Admin = 1,
            User = 2
        };
    }
}
