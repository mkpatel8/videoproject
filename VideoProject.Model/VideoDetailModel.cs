﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Model
{
    public class VideoDetailModel
    {
        public VideoCampaignModel vcm { get; set; }
        public List<VideoHierarchyModel> lstVideoHierarchy { get; set; }
    }

    public class VideoCampaignModel
    {
        public int VideoCampaignID { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public int? QuestionDuration { get; set; }
        public string CreatedDate { get; set; }
        public string QueBG { get; set; }
        public string QueFont { get; set; }
        public string AnsBG { get; set; }
        public string AnsFont { get; set; }
        public string AnsHoverBG { get; set; }
        public string AnsHoverFont { get; set; }
        public string URL { get; set; }
        public string PathsJson { get; set; }
        public string ElementsJson { get; set; }
        public string SourcesJson { get; set; }
        public string ComposerData { get; set; }
        public int NewVideoCampaignID { get; set; }
    }

    public class VideoHierarchyModel
    {
        public int VideoID { get; set; }
        public bool IsStart { get; set; }
        public string VideoURL { get; set; }
        public int QuestionID { get; set; }
        public string QueDetail { get; set; }
        public int AnswerID { get; set; }
        public string AnsDetail { get; set; }
        public int? NextVideoID { get; set; }
        public string RedirectVideoText { get; set; }
        public string RedirectVideoType { get; set; }
    }

    public class VideoHierarchySaveModel
    {
        public int selfID { get; set; }
        public int? parentID { get; set; }
        public string QuestionText { get; set; }
        public string AnswerText { get; set; }
        public string VideoUrl { get; set; }
        public int? VideoId { get; set; }
        public int? QuestionId { get; set; }
        public int? AnswerId { get; set; }
        public string RedirectVideoText { get; set; }
        public string RedirectVideoType { get; set; }
    }
    public class VideoModel
    {
        public VideoModel()
        {
            VideoElements = new List<VideoElement>();
        }
        public string URL { get; set; }
        public int VideoID { get; set; }
        public int VideoCampaignID { get; set; }
        public string UniqueID { get; set; }
        public string VideoType { get; set; }
        public string VideoURLID { get; set; }
        public List<selectList> selectLists = new List<selectList>();
        public List<VideoElement> VideoElements = new List<VideoElement>();
    }
    public class selectList
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
    }
    public class VideoElement
    {
        public VideoElement()
        {
            ElementID = 0;
            VideoID = 0;
            ElementType = 0;
            ElementHeight = 0;
            ElementWidth = 0;
            ElementPositionX = 0;
            ElementPositionY = 0;
            Timein = 0;
            TimeOut = 0;
            pauseWhileShow = false;
        }
        public int ElementID { get; set; }
        public int VideoID { get; set; }
        public string ElementName { get; set; }
        public int ElementType { get; set; }
        public int ElementHeight { get; set; }
        public int ElementWidth { get; set; }
        public int ElementPositionX { get; set; }
        public int ElementPositionY { get; set; }
        public double Timein { get; set; }
        public double TimeOut { get; set; }
        public string Action { get; set; }
        public string ActionItem { get; set; }
        public bool pauseWhileShow { get; set; }
        public string FontColor { get; set; }
        public int FontSize { get; set; }
        public string UniqueID { get; set; }
        public string BackColor { get; set; }
    }
}
