﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Model
{
    public class UserDetailModel
    {
        [Required(ErrorMessage = "*")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*")]
        public string UserPassword { get; set; }

        [Required(ErrorMessage = "*")]
        [Compare("UserPassword")]
        public string ConfirmPassword { get; set; }

        public int UserId { get; set; }

        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                           @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                           @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                           ErrorMessage = "Email is not valid")]
        [System.Web.Mvc.Remote("IsEmail_Available", "Login", ErrorMessage = "Already available")]
        public string UserEmail { get; set; }


        public int RoleId { get; set; }

        public string RoleName { get; set; }

    }
}
