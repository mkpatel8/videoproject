﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Model
{
    public class ButtonSetModel
    {
        public int ButtonSetID { get; set; }
        public string ButtonSetName { get; set; }
        public int UserID { get; set; }
        public bool IsDisplayHorizontal { get; set; }
        public List<ButtonModel> lstButtons { get; set; }
    }

    public class ButtonModel
    {
        public int ButtonID { get; set; }
        public string ButtonText { get; set; }
        public string ButtonBackgroundColor { get; set; }
        public string ButtonFontColor { get; set; }
        public string ButtonHoverBackgroundColor { get; set; }
        public string ButtonHoverFontColor { get; set; }
        public string ButtonLink { get; set; }
    }

}
