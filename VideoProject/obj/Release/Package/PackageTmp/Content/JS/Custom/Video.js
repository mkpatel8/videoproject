﻿

// create youtube player
var YoutubePlayer;
function onYouTubePlayerAPIReady() {
    YoutubePlayer = new YT.Player('YoutubePlayer', {
        videoId: '',
        events: {
            'onReady': onYoutubePlayerReady,
            'onStateChange': onYoutubePlayerStateChange
        }
    });
}

// autoplay youtube video
function onYoutubePlayerReady(event) {

    if ($("#YoutubePlayer:visible").length = 0) {

    }
    else {
        event.target.playVideo();
    }
}

// when youtube video ends
function onYoutubePlayerStateChange(event) {
    if (event.data === 0) {
        $('#YoutubePlayer').hide();
        $("#div_que").show();
    }
}

// get YouTube Video id From YouTube Url
function getYouTubeVideoID(YouTubeURL) {
    var YTVideoId;

    if (YouTubeURL.indexOf("youtube.com") != -1) {
        YTVideoId = YouTubeURL.substr(YouTubeURL.lastIndexOf("?v=")).substr(3);
    }
    else if (YouTubeURL.indexOf("youtu.be") != -1) {
        YTVideoId = YouTubeURL.substr(YouTubeURL.lastIndexOf("/")).substr(1);
    }

    return YTVideoId;
}

// get Vimeo Video id From Vimeo Url
function getVimeoVideoID(VimeoURL) {
    var VimeoVideoId = VimeoURL.substr(VimeoURL.lastIndexOf("/")).substr(1);

    var checkquerystr = VimeoVideoId.lastIndexOf("?");

    if (checkquerystr != -1) {
        VimeoVideoId = VimeoVideoId.substr(0, checkquerystr);
    }

    return VimeoVideoId;
}

//Play Video Page Js Start
var VideoHierarchyModel;
var VideoDetailModel;
var lstVideoHierarchy;
var que_inline_css, ans_inline_css;

function PlayVideo(VideoURL) {

    if (VideoURL.length > 0) {

        SetVideoQuestion();

        if (VideoURL.indexOf("youtube.com") != -1 || VideoURL.indexOf("youtu.be") != -1) {

            $('#VimeoPlayer').remove();

            var YTVideoId = getYouTubeVideoID(VideoURL);

            if (typeof YoutubePlayer == typeof undefined) {

                if (typeof YT.Player == typeof undefined) {
                    setTimeout(function () {
                        YoutubePlayer.loadVideoById(YTVideoId, 0, "Auto");
                        $('#YoutubePlayer').show();
                        SetVideoQuestionTime();
                    }, 5000);
                }
                else {
                    YoutubePlayer.loadVideoById(YTVideoId, 0, "Auto");
                    $('#YoutubePlayer').show();
                    SetVideoQuestionTime();
                }
            }
            else {

                if (typeof YoutubePlayer.loadVideoById == typeof undefined) {
                    YoutubePlayer = new YT.Player('YoutubePlayer', {
                        videoId: YTVideoId,
                        events: {
                            'onReady': onYoutubePlayerReady,
                            'onStateChange': onYoutubePlayerStateChange
                        }
                    });

                    $('#YoutubePlayer').show();
                    SetVideoQuestionTime();
                }
                else {
                    YoutubePlayer.loadVideoById(YTVideoId, 0, "Auto");
                    $('#YoutubePlayer').show();
                    SetVideoQuestionTime();
                }
            }

        }
        else if (VideoURL.indexOf("vimeo.com") != -1) {

            $('#YoutubePlayer').hide();

            if (typeof YoutubePlayer != typeof undefined) {
                if (typeof YoutubePlayer.stopVideo != typeof undefined) {
                    YoutubePlayer.stopVideo();
                }
            }

            var VimeoVideoId = getVimeoVideoID(VideoURL);
            $('#VimeoPlayer').remove();
            var IFrame = "<iframe id='VimeoPlayer' class='embed-responsive-item' style='display: none;' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
            $("#mainBodyDiv").append(IFrame);
            $('#VimeoPlayer').attr("src", "https://player.vimeo.com/video/" + VimeoVideoId);

            $('#VimeoPlayer').show();

            var VimeoPlayer = new Vimeo.Player($('#VimeoPlayer'));
            VimeoPlayer.play();

            VimeoPlayer.on('loaded', function (data) {
                SetVideoQuestionTime();
            });

            VimeoPlayer.on('ended', function () {
                $('#VimeoPlayer').hide();
                $("#div_que").show();
            });
        }
    }
}

function SetVideoQuestionTime() {
    if (VideoDetailModel.vcm.QuestionDuration > 0) {
        setTimeout(function () {
            $("#div_que").show();
        }, ((VideoDetailModel.vcm.QuestionDuration + 4) * 1000));
    }
}

function SetVideoQuestion() {

    $("#div_que").empty();

    if (VideoHierarchyModel['QuestionID'] > 0) {

        setQueAnsInlineStyle(VideoDetailModel.vcm);

        var div_que = "<div class='questionText'>"
                      + "<div " + que_inline_css + " class='questionLabel'>"
                      + "<a class='closeButton'></a><label>" + VideoHierarchyModel['QueDetail'] + "</label></div></div><div>";

        $.each(lstVideoHierarchy, function () {
            if (this['VideoID'] == VideoHierarchyModel['VideoID']) {

                if (this['NextVideoID'] > 0) {
                    div_que += "<button " + ans_inline_css + " onclick='PlayAnswerVideo(" + this['NextVideoID'] + ");'>"
                                + this['AnsDetail'] + "</button>";
                }
                else if (this['RedirectVideoType'] == "Link" || this['RedirectVideoType'] == "Subscribe" || this['RedirectVideoType'] == "PostReview") {
                    div_que += "<a " + ans_inline_css + " href='//" + this['RedirectVideoText'] + "'>"
                                + this['AnsDetail'] + "</a>";
                }
                else if (this['RedirectVideoType'] == "Contact") {
                    div_que += "<a " + ans_inline_css + " href='tel:" + this['RedirectVideoText'] + "'>"
                                + this['AnsDetail'] + "</a>";
                }
                else if (this['RedirectVideoType'] == "Skype") {
                    div_que += "<a " + ans_inline_css + " href='skype:" + this['RedirectVideoText'] + "?call'>"
                                + this['AnsDetail'] + "</a>";
                }
            }
        });

        div_que += "</div>";

        $("#div_que").html(div_que);
    }
}

function PlayAnswerVideo(NextVideoID) {

    $("#div_que").empty();
    $("#div_que").hide();

    if (NextVideoID > 0) {
        $.each(lstVideoHierarchy, function () {
            if (this['VideoID'] == NextVideoID) {
                VideoHierarchyModel = this;
                PlayVideo(VideoHierarchyModel['VideoURL']);
                return false;
            }
        });
    }
}

function StopVideoes() {
    $('#VimeoPlayer').remove();

    $('#YoutubePlayer').hide();

    if (typeof YoutubePlayer != typeof undefined) {
        if (typeof YoutubePlayer.stopVideo != typeof undefined) {
            YoutubePlayer.stopVideo();
        }
    }

    $("#div_que").empty();
    $("#div_que").hide();
}

function setQueAnsInlineStyle(vcm) {

    if (vcm.QueBG == "") {
        vcm.QueBG += "lightskyblue";
    }

    if (vcm.QueFont == "") {
        vcm.QueFont += "#292b2c";
    }

    que_inline_css = 'style="background-color: ' + vcm.QueBG + ';color: ' + vcm.QueFont + ';"';

    //answer
    if (vcm.AnsBG == "") {
        vcm.AnsBG = "#ffffff";
    }

    if (vcm.AnsFont == "") {
        vcm.AnsFont = "#1d89ff";
    }

    if (vcm.AnsHoverBG == "") {
        vcm.AnsHoverBG = "#1d89ff";
    }
    if (vcm.AnsHoverFont == "") {
        vcm.AnsHoverFont = "#ffffff";
    }

    if (vcm.AnsBG == "#ffffff" && vcm.AnsFont == "#1d89ff" && vcm.AnsHoverBG == "#1d89ff" && vcm.AnsHoverFont == "#ffffff") {
        ans_inline_css = " class='row bttn-unite bttn-md bttn-primary answerText' style='border-radius: initial;'";
    }
    else {
        ans_inline_css = 'style = "color: ' + vcm.AnsFont + '; background-color: ' + vcm.AnsBG + ';border-radius: initial;"';
        ans_inline_css += " class='row bttn-md bttn-primary answerText'";
        ans_inline_css += " onmouseover='$(this).attr(\"style\", \"color: " + vcm.AnsHoverFont + "; background-color: " + vcm.AnsHoverBG + ";border-radius: initial;\")'";
        ans_inline_css += " onmouseout='$(this).attr(\"style\", \"color: " + vcm.AnsFont + "; background-color: " + vcm.AnsBG + ";border-radius: initial;\")'";
    }
}
//Play Video Page Js End