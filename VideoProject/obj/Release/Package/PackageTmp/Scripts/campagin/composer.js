(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (global = global || self, factory(global.Composer = {}));
}(this, (function (exports) { 'use strict';

    function removeFromParent(node) {
        const parent = node.parentNode;
        if (parent)
            parent.removeChild(node);
    }
    function reattachNode(node) {
        const parent = node.parentNode;
        if (parent)
            parent.appendChild(node);
    }
    function template(source) {
        const template = document.createElement("template");
        template.innerHTML = source.trim();
        return template;
    }
    function addEventHandler(element, name, handler) {
        element.addEventListener(name, handler);
    }
    function boolClass(element, name, value) {
        element.classList[value ? "add" : "remove"](name);
    }
    function px(n) {
        return `${n}px`;
    }
    function removeFromList(list, item) {
        const index = list.indexOf(item);
        if (~index)
            list.splice(index, 1);
    }

    class Component {
        constructor(name, $1, $2) {
            const tag = arguments.length === 3 ? $2 : typeof $1 === "string" ? $1 : "div";
            const template = $1 && typeof $1 !== "string" ? $1 : null;
            const main = this.main = this.create(tag);
            if (template)
                main.appendChild(template.content.cloneNode(true));
            main.classList.add(...name.split(" "));
        }
        create(tag) {
            return document.createElement(tag);
        }
        select(elements) {
            const main = this.main;
            for (const id of Object.getOwnPropertyNames(elements)) {
                // @ts-ignore
                this[id] = main.querySelector(elements[id]);
            }
    }
    attach(node) {
        node.appendChild(this.main);
        return this;
    }
    detach() {
        removeFromParent(this.main);
        return this;
    }
}

    var vec;
(function (vec) {
    function create(x, y) {
        return [x, y];
    }
    vec.create = create;
    function set(out, x, y) {
        out[0] = x;
        out[1] = y;
        return out;
    }
    vec.set = set;
    function add(out, a, b) {
        out[0] = a[0] + b[0];
        out[1] = a[1] + b[1];
        return out;
    }
    vec.add = add;
    function sub(out, a, b) {
        out[0] = a[0] - b[0];
        out[1] = a[1] - b[1];
        return out;
    }
    vec.sub = sub;
    function sub_inline(out, ax, ay, bx, by) {
        out[0] = ax - bx;
        out[1] = ay - by;
        return out;
    }
    vec.sub_inline = sub_inline;
    function sub_inline_b(out, u, x, y) {
        out[0] = u[0] - x;
        out[1] = u[1] - y;
        return out;
    }
    vec.sub_inline_b = sub_inline_b;
    function sub_inline_a(out, x, y, u) {
        out[0] = x - u[0];
        out[1] = y - u[1];
        return out;
    }
    vec.sub_inline_a = sub_inline_a;
    function lerp(out, a, b, t) {
        out[0] = b[0] * t + a[0] * (1 - t);
        out[1] = b[1] * t + a[0] * (1 - t);
        return out;
    }
    vec.lerp = lerp;
})(vec || (vec = {}));

let offset = 0;
    const size = 31;
function index() {
    const o = offset++;
    if (offset === size) {
        reseed();
        offset = 0;
    }
    return o;
}
    const buffer = new Uint8Array(size);
    const reseed = (crypto ?
        function reseed() { crypto.getRandomValues(buffer); } :
        function reseed() {
            for (let i = 0; i < buffer.length; i++) {
                buffer[i] = (Math.random() * 256) | 0;
            }
        });
reseed();
    const template$1 = '10000000-1000-4000-8000-100000000000';
    const matcher = /[018]/g;
function toByteString(d) {
    return (((d ^ buffer[index()]) & 15) >> (d >> 2)).toString(16);
}
function parseCharacter(c) {
    return toByteString(c.charCodeAt(0) - 48);
}
function generateUUID() {
    return template$1.replace(matcher, parseCharacter);
}

    const modal_template = template(`
<div class="modal-dialog" role="document">
  <form class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Edit Connection</h5>
      <button type="button" class="close" data-dismiss="modal">
        <span>&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group">
        <label for="composer-answer" class="col-form-label">Answer:</label>
        <input type="text" class="form-control" id="composer-answer">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-danger">Delete</button>
      <button type="submit" class="btn btn-primary">Save</button>
    </div>
  </form>
</div>`);
    const EditConnection = new class extends Component {
        constructor() {
            super("modal", modal_template);
            this.connection = undefined;
            this.main.classList.add("fade");
            this.select({
    form: ".modal-content",
    answer: "#composer-answer",
                delete: ".btn-danger",
        });
            addEventHandler(this.form, "submit", this);
            addEventHandler(this.delete, "click", this);
            $(this.main)
                .on("shown.bs.modal", this.shown.bind(this))
                .on("hidden.bs.modal", this.hidden.bind(this));
        }
        shown() {
            this.answer.select();
        }
        hidden() {
            this.detach();
        }
        show() {
            this.attach(document.body);
            this.answer.value = this.connection.answer;
            $(this.main).modal("show");
        }
        handleEvent(e) {
            e.preventDefault();
            if (e.type === "submit") {
                this.connection.answer = this.answer.value;
        }
        else {
                this.connection.confirmDelete();
        }
            $(this.main).modal("hide");
            this.connection = undefined;
        }
        };

    const modal_template$1 = template(`
<div class="modal-dialog" role="document">
  <form class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Delete <span id="composer-type"></span></h5>
      <button type="button" class="close" data-dismiss="modal">
        <span>&times;</span>
      </button>
    </div>
    <div class="modal-body">Do you really want to delete this <span id="composer-type-body"></span> ?</div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-secondary" data-dismiss="modal">No</button>
      <button type="button" class="btn btn-danger">Yes</button>
    </div>
  </form>
</div>`);
    const DisposeConfirm = new class extends Component {
        constructor() {
            super("modal", modal_template$1);
            this.disposable = undefined;
            this.main.classList.add("fade");
            this.main.tabIndex = -1;
            this.select({
            form: ".modal-content",
                delete: ".btn-danger",
            cancel: ".btn-secondary",
            title: "#composer-type",
            body: "#composer-type-body",
        });
            addEventHandler(this.form, "submit", this);
            addEventHandler(this.delete, "click", this);
            $(this.main)
                .on("shown.bs.modal", this.shown.bind(this))
                .on("hidden.bs.modal", this.hidden.bind(this));
        }
        shown() {
            this.cancel.focus();
        }
        hidden() {
            this.detach();
        }
        show() {
            this.attach(document.body);
            let kind = this.disposable.constructor.name;
            if (kind !== "Connection") {
            // @ts-ignore
                const type = this.disposable.type;
                kind = type === "Video" ? "Element" : type + " Element";
        }
            this.body.textContent = this.title.textContent = kind;
            $(this.main).modal("show");
        }
        handleEvent(e) {
            e.preventDefault();
            if (e.type === "click") {
                this.disposable.dispose();
        }
            $(this.main).modal("hide");
        }
        };

    class Connection extends Component {
        constructor(composer, answer, from, to) {
            super("composer-connection", "path");
            this.composer = composer;
            this.answer = answer;
            this.from = from;
            this.to = to;
            this.uuid = generateUUID();
            from.connections.push(this);
            to.connections.push(this);
            composer.connections.add(this);
            addEventHandler(this.main, "click", this);
            this.recalculatePath();
        }
        showEditModal() {
            EditConnection.connection = this;
            EditConnection.show();
        }
        confirmDelete() {
            DisposeConfirm.disposable = this;
            DisposeConfirm.show();
        }
        handleEvent(e) {
            if (e.shiftKey) {
                this.confirmDelete();
        }
        else {
                this.showEditModal();
        }
        }
        create(tag) {
            return document.createElementNS("http://www.w3.org/2000/svg", tag);
        }
        static connect(composer, from, to) {
            if (!Connection.findConnection(from, to))
                return new Connection(composer, "", from, to);
            return null;
        }
        static findConnection(from, to) {
            for (const connection of from.connections) {
                if (connection.to === to)
                    return connection;
        }
            return null;
        }
        recalculatePath() {
            this.main.setAttribute("d", this.composer.path(this.from.position, this.to.position));
        }
        dispose() {
            removeFromList(this.from.connections, this);
            removeFromList(this.to.connections, this);
            this.composer.connections.delete(this);
            this.detach();
        }
        toJSON() {
            return {
            From: this.from.element.uuid,
            To: this.to.element.uuid,
            Answer: this.answer
        };
        }
        static fromJSON(composer, data) {
            return new Connection(composer, data.Answer, composer.elements.get(data.From)[1], composer.elements.get(data.To)[0]);
        }
        }

    const socket_offsets = [
        vec.create(-6, 64),
        vec.create(134, 64)
        ];
    const socket_classes = [
        "composer-socket-left",
        "composer-socket-right"
        ];
    const drag_offset = vec.create(0, 0);
    const mouse = vec.create(0, 0);
    let from, to;
    class Socket extends Component {
        constructor(composer, element, type) {
            super(`composer-socket ${socket_classes[type]} fas fa-angle-right`, "i");
            this.composer = composer;
            this.element = element;
            this.type = type;
            this.position = vec.create(0, 0);
            this.uuid = generateUUID();
            this.connections = [];
            this.dragging = false;
            this.condidate = null;
            composer.sockets.add(this);
            this.preview = composer.connection_preview;
            const main = this.main;
            main.socket = this;
            addEventHandler(main, "pointerdown", this);
            addEventHandler(main, "pointermove", this);
            addEventHandler(main, "pointerup", this);
            addEventHandler(main, "mouseup", this);
            this.onPositionChange();
        }
        updateCondidateSocket(socket) {
            const condidate = this.condidate;
            if (condidate === socket)
                return;
            if (condidate)
                condidate.main.classList.remove('condidate');
            if (socket)
                socket.main.classList.add('condidate');
            this.condidate = socket;
        }
        handleEvent(e) {
            e.preventDefault();
            const target = e.target;
            switch (e.type) {
                case "pointerdown":
                    this.main.classList.add("connecting");
                    this.composer.setDraggingStatus(true);
                    if (e.pointerId)
                        target.setPointerCapture(e.pointerId);
                    const position = this.position;
                    vec.sub_inline_a(drag_offset, e.pageX, e.pageY, position);
                    if (this.type === 1 /* Out */) {
                        from = position;
                        to = mouse;
        }
        else {
                        from = mouse;
                        to = position;
        }
                    this.dragging = true;
                    break;
                case "pointermove":
                    if (this.dragging) {
                        vec.sub_inline_a(mouse, e.pageX, e.pageY, drag_offset);
                        this.preview.setAttribute("d", this.composer.path(from, to));
                        this.onPositionChange();
                        this.updateCondidateSocket(this.socketAt(e.clientX, e.clientY));
        }
                    break;
                case "pointerup":
                    this.main.classList.remove("connecting");
                    target.releasePointerCapture(e.pointerId);
                    this.composer.setDraggingStatus(false);
                    this.preview.setAttribute("d", "M0 0 ");
                    const socket = this.socketAt(e.clientX, e.clientY);
                    if (socket) {
                        let from, to;
                        if (this.type === 1 /* Out */) {
                            from = this;
                            to = socket;
        }
        else {
                            from = socket;
                            to = this;
        }
                        const connection = Connection.connect(this.composer, from, to);
                        if (connection)
                            connection.showEditModal();
        }
                    this.dragging = false;
        }
            return false;
        }
        socketAt(x, y) {
            const element = document.elementFromPoint(x, y);
            if (element && this.validateSocket(element.socket))
                return element.socket;
            const elements = document.elementsFromPoint(x, y);
            for (const element of elements) {
                const socket = element.socket;
                if (this.validateSocket(socket))
                    return socket;
        }
            return null;
        }
        validateSocket(socket) {
            return (socket &&
                socket.type !== this.type &&
                socket.element !== this.element);
        }
        onPositionChange() {
            const position = this.position;
            vec.add(position, socket_offsets[this.type], this.element.position);
            const style = this.main.style;
            style.left = px(position[0]);
            style.top = px(position[1]);
            for (const connection of this.connections)
                connection.recalculatePath();
        }
        onStartChange(status) {
            boolClass(this.main, "composer-start", status);
        }
        dispose() {
            const connections = this.connections;
            for (let i = connections.length - 1; i >= 0; i--) {
                const connection = connections[i];
                connection.dispose();
        }
            this.composer.sockets.delete(this);
            this.detach();
        }
        }

    const modal_template$2 = template(`
<div class="modal-dialog" role="document">
  <form class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Edit Element</h5>
      <button type="button" class="close" data-dismiss="modal">
        <span>&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group" id="composer-name-field">
        <label for="composer-name" class="col-form-label">Name:</label>
        <input type="text" class="form-control" id="composer-name">
      </div>
      <div class="form-group">
        <label for="composer-question" class="col-form-label" id="composer-question-label">Question:</label>
        <input type="text" class="form-control" id="composer-question">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-danger">Delete</button>
      <button type="submit" class="btn btn-primary">Save</button>
    </div>
  </form>
</div>`);
    const question_labels = {
        "Video": "Question",
        "Skype": "Skype Name",
        "Link": "URL",
        "Phone": "Phone Number"
        };
    const question_placeholder = {
        "Video": "Your question here ...",
        "Skype": "A skype name here ...",
        "Link": "A web URL here ...",
        "Phone": "A phone number ..."
        };
    const EditElement = new class extends Component {
        constructor() {
            super("modal", modal_template$2);
            this.element = undefined;
            this.main.classList.add("fade");
            this.select({
            form: ".modal-content",
            title: ".modal-title",
            question: "#composer-question",
            name: "#composer-name",
            name_field: "#composer-name-field",
            question_label: "#composer-question-label",
                delete: ".btn-danger",
        });
            addEventHandler(this.form, "submit", this);
            addEventHandler(this.delete, "click", this);
            $(this.main)
                .on("shown.bs.modal", this.shown.bind(this))
                .on("hidden.bs.modal", this.hidden.bind(this));
        }
        shown() {
            this.question.select();
        }
        hidden() {
            this.detach();
        }
        show() {
            this.attach(document.body);
            const element = this.element;
            const type = element.type;
            this.name_field.hidden = type !== "Video";
            this.question.placeholder = question_placeholder[type];
            this.question_label.textContent = question_labels[type];
            this.title.textContent = "Edit " + (type === "Video" ? "Element" : type + " Element");
            this.question.value = element.question;
            this.name.value = element.name;
            $(this.main).modal("show");
        }
        handleEvent(e) {
            e.preventDefault();
            const element = this.element;
            if (e.type === "submit") {
                const question = this.question.value;
                const thChange = element.question !== question;
                element.question = question;
                element.name = this.name.value;
                element.onTextUpdate();
                if (thChange)
                    element.onThumbnailChange();
        }
        else {
                element.confirmDipose();
        }
            $(this.main).modal("hide");
            this.element = undefined;
        }
        };

    const drag_offset$1 = vec.create(0, 0);
    const element_template = template(`
<div class="composer-element--name"></div>
<div class="composer-element--thumbnail">
  <div class="composer-element--question"></div>
</div>
<div class="composer-element--actions">
  <i
    class="composer-element--start fas fa-star"
    data-action="start"
    title="Make starting element"
  ></i>
  <i data-action="edit" title="Edit element" class="composer-element--edit fas fa-edit"></i>
  <i data-action="clone" title="Clone element" class="composer-element--clone fas fa-clone"></i>
  <i data-action="delete" title="Delete element" class="composer-element--delete fas fa-trash-alt"></i>
</div>`);
    const element_selectors = {
        "name_element": ".composer-element--name",
        "quest_element": ".composer-element--question",
        "thumbnail": ".composer-element--thumbnail",
        };
    const TypeMap = {
        "V": "Video",
        "S": "Skype",
        "L": "Link",
        "P": "Phone",
        };
    class Element extends Component {
        constructor(composer, type, uuid, source, name, question, x, y) {
            super(`composer-element composer-${type.toLowerCase()}-element`, element_template);
            this.composer = composer;
            this.type = type;
            this.uuid = uuid;
            this.source = source;
            this.name = name;
            this.question = question;
            this.sockets = this;
            this.actions = {
                start() {
                    this.composer.setStart(this);
        },
                delete() {
                    this.confirmDipose();
        },
                clone(dx, dy) {
                    new Element(this.composer, this.type, generateUUID(), this.source, this.name, this.question, this.position[0] + dx - 32, this.position[1] + dy - 32);
        },
                edit() {
                    this.showEditModal();
        }
        };
            this.dragging = false;
            this.position = vec.create(x, y);
            this.select(element_selectors);
            const main = this.main;
            this[0] = new Socket(composer, this, 0 /* In */);
            if (type === "Video")
                this[1] = new Socket(composer, this, 1 /* Out */);
            composer.elements.add(this);
            this.thumbnail.insertBefore(this.image = this.getImageForType(type), this.quest_element);
            this.onPositionChange();
            this.onTextUpdate();
            addEventHandler(main, "pointerdown", this);
            addEventHandler(main, "pointermove", this);
            addEventHandler(main, "pointerup", this);
            addEventHandler(main, "click", this);
        }
        getImageForType(type) {
            switch (type) {
            // @ts-ignore
                case "Video": return this.source.image.cloneNode();
            // @ts-ignore
                default: return this.composer[`default${type}Image`].cloneNode();
        }
        }
        confirmDipose() {
            DisposeConfirm.disposable = this;
            DisposeConfirm.show();
        }
        showEditModal() {
            EditElement.element = this;
            EditElement.show();
        }
        onTextUpdate() {
            this.name_element.title = this.name_element.textContent = this.name;
            this.quest_element.title = this.quest_element.textContent = this.question;
        }
        onThumbnailChange() {
            if (this.type !== "Video") {
                this.composer.onSpecialElementUpdate(this);
        }
        }
        onPositionChange() {
            const position = this.position;
            const style = this.main.style;
            style.left = px(position[0]);
            style.top = px(position[1]);
            this[0].onPositionChange();
            if (this.type === "Video")
                this[1].onPositionChange();
        }
        onStartChange(status) {
            boolClass(this.main, "composer-start", status);
            this[0].onStartChange(status);
            if (this.type === "Video")
                this[1].onStartChange(status);
        }
        handleEvent(e) {
            e.preventDefault();
            const target = e.target;
            const position = this.position;
            if (target.socket)
                return;
            if (target.tagName === "I") {
                e.stopPropagation();
                if (e.type !== "click")
                    return;
                this.actions[target.dataset["action"]].call(this, e.pageX - position[0], e.pageY - position[1]);
                return;
        }
            switch (e.type) {
                case "pointerdown":
                    reattachNode(this.main);
                    this.composer.setDraggingStatus(true);
                    target.setPointerCapture(e.pointerId);
                    vec.sub_inline_a(drag_offset$1, e.pageX, e.pageY, position);
                    this.dragging = true;
                    break;
                case "pointermove":
                    if (this.dragging) {
                        vec.sub_inline_a(position, e.pageX, e.pageY, drag_offset$1);
                        this.onPositionChange();
        }
                    break;
                case "pointerup":
                    target.releasePointerCapture(e.pointerId);
                    this.composer.setDraggingStatus(false);
                    this.dragging = false;
        }
            return false;
        }
        static fromJSON(composer, data) {
            const type = TypeMap[data.Type];
            const SourceID = data.SourceID;
            const element = new Element(composer, type, data.ID, SourceID === "" ? null : composer.sources.get(SourceID), type === "Video" ? data.Name : type, data.Question, parseInt(data.Left), parseInt(data.Top));
            if (data.IsStart && type === "Video")
                composer.setStart(element);
            return element;
        }
        toJSON() {
            const position = this.position;
            const source = this.source;
            return {
            ID: this.uuid, Type: this.type.charAt(0),
            SourceID: source ? source.uuid : "",
            IsStart: this.composer.isStart(this),
            Left: px(position[0]),
            Top: px(position[1]),
            Question: this.question,
            Name: source ? this.name : ""
        };
        }
        dispose() {
            if (this.composer.isStart(this))
                this.composer.removeStart();
            this[0].dispose();
            if (this.type === "Video")
                this[1].dispose();
            this.composer.elements.delete(this);
            this.detach();
        }
        }

    const source_template = template(`
<div class="composer-source--thumbnail">
  <i class="fas fa-spinner fa-pulse"></i>
</div>
<div class="composer-source--label"></div>
<i class="fas fa-eye"></i>`);
    const drag_offset$2 = vec.create(0, 0);
    class Source extends Component {
        constructor(composer, uuid, title, source, url) {
            super("composer-source", source_template);
            this.composer = composer;
            this.uuid = uuid;
            this.title = title;
            this.source = source;
            this.url = url;
            this.dragging = false;
            this.element = undefined;
            const image = this.image = new Image();
            this.select({
            label: ".composer-source--label",
            thumbnail: ".composer-source--thumbnail"
        });
            this.thumbnail.appendChild(image);
            this.label.textContent = title;
            this.composer.sources.add(this);
            const main = this.main;
            addEventHandler(main, "pointerdown", this);
            addEventHandler(main, "pointermove", this);
            addEventHandler(main, "pointerup", this);
            addEventHandler(image, "load", this);
            image.src = source;
        }
        handleEvent(e) {
            e.preventDefault();
            const target = e.target;
            const element = this.element;
            switch (e.type) {
                case "load":
                    this.thumbnail.classList.add("loaded");
                    break;
                case "pointerdown":
                    if (target.tagName === 'I')
                        return;
                    this.composer.setDraggingStatus(true);
                    target.setPointerCapture(e.pointerId);
                    this.dragging = true;
                    break;
                case "pointermove":
                    if (element) {
                        vec.sub_inline_a(element.position, e.pageX - 32, e.pageY - 32, drag_offset$2);
                        element.onPositionChange();
        }
        else if (this.dragging) {
                        const [x, y] = this.composer.getTopLeft();
                        vec.set(drag_offset$2, x, y);
                        this.composer.setPanelDisplay(false);
                        const start = this.composer.shouldNextElementBeStart();
                        const element = this.element = new Element(this.composer, "Video", generateUUID(), this, this.title, "", 0, 0);
                        if (start)
                            this.composer.setStart(element);
        }
                    break;
                case "pointerup":
                    target.releasePointerCapture(e.pointerId);
                    this.composer.setDraggingStatus(false);
                    if (element) {
                        this.composer.setPanelDisplay(true);
                        if (this.composer.editElementAfterDrop)
                            element.showEditModal();
                        this.element = undefined;
        }
                    this.dragging = false;
        }
        }
        static fromJSON(composer, data) {
            return new Source(composer, data.ID, data.Title, data.Thumbnail, data.URL);
        }
        toJSON() {
            return {
            ID: this.uuid,
            Title: this.title,
            Thumbnail: this.source,
            URL: this.url
        };
        }
        dispose() {
            this.composer.sources.delete(this);
            this.detach();
        }
        }

    class Collection {
        constructor(context, type, listener = {}) {
            this.context = context;
            this.list = [];
            this.dict = Object.create(null);
            this.type = type.name;
            this.onAdd = listener.onAdd || this.onAdd;
            this.onRemove = listener.onRemove || this.onRemove;
            this.onClear = listener.onClear || this.onClear;
        }
        onAdd() { }
        onRemove() { }
        onClear() { }
        get length() { return this.list.length; }
        get(uuid) {
            const dict = this.dict;
            const item = dict[uuid];
            if (item == null)
                throw new TypeError(`${this.type} does not exit.`);
            return item;
        }
        add(item) {
            const dict = this.dict;
            const list = this.list;
            const uuid = item.uuid;
            const current = dict[uuid];
            if (current == null) {
            //@ts-ignore
                list.push(item);
                dict[uuid] = item;
                this.onAdd.call(this.context, item);
                return item;
        }
            throw new TypeError(`Another ${this.type} already exists.`);
        }
        delete(item) {
            const dict = this.dict;
            const list = this.list;
            const uuid = item.uuid;
            const saved = dict[uuid];
            if (saved == null)
                throw new Error(`${this.type} does not exist.`);
            if (saved === item) {
                this.onRemove.call(this.context, item);
                delete dict[uuid];
            //@ts-ignore
                list.splice(list.indexOf(saved), 1);
                return item;
        }
            throw new TypeError(`Saved ${this.type} is different from provided ${this.type}`);
        }
        remove(uuid) {
            const dict = this.dict;
            const list = this.list;
            const item = dict[uuid];
            if (item == null)
                throw new Error(`${this.type} does not exist.`);
            this.onRemove.call(this.context, item);
            delete dict[uuid];
            //@ts-ignore
            list.splice(list.indexOf(item), 1);
            return item;
        }
        clear() {
            this.onClear.call(this.context);
            const dict = this.dict;
            for (const name of Object.keys(dict))
                delete dict[name];
            //@ts-ignore
            this.list.length = 0;
        }
        }

    function path(name) {
        return (path[name] || path.line).plot;
        }
    (function (path) {
        let line;
        (function (line) {
            const offset = 32;
            function plot(s, e) {
                const sx = s[0];
                const ex = e[0];
                const sy = s[1];
                const ey = e[1];
                const m = Math.max((ex - sx) * .5, offset);
                const sox = sx + m;
                const eox = ex - m;
                return `M${sx} ${sy} L${sox} ${sy} L${eox} ${ey} L${ex} ${ey}`;
        }
            line.plot = plot;
        })(line = path.line || (path.line = {}));
        let bezier;
        (function (bezier) {
            const so = vec.create(0, 0);
            const eo = vec.create(0, 0);
            const offset = vec.create(32, 0);
            const t0 = .5;
            const t1 = .5;
            function lerp_f(a, b, t) {
                return b * t + a * (1 - t);
        }
            function plot(s, e) {
                vec.add(so, s, offset);
                vec.sub(eo, e, offset);
                const sx = s[0];
                const sox = so[0];
                const eox = eo[0];
                const ex = e[0];
                const sy = s[1];
                const soy = so[1];
                const eoy = eo[1];
                const ey = e[1];
                const mx = lerp_f(sox, eox, t0);
                const my = lerp_f(soy, eoy, t1);
                if ((eox - sox) > 0)
                    return `M${sx} ${sy} L${sox} ${soy} C${mx} ${soy} ${mx} ${eoy} ${eox} ${eoy} L${ex} ${ey}`;
        else
                    return `M${sx} ${sy} L${sox} ${soy} C${sox} ${my} ${eox} ${my} ${eox} ${eoy} L${ex} ${ey}`;
        }
            bezier.plot = plot;
        })(bezier = path.bezier || (path.bezier = {}));
        })(path || (path = {}));

    const modal_template$3 = template(`
<div class="modal-dialog" role="document">
  <form class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Upload Video</h5>
      <button type="button" class="close" data-dismiss="modal">
        <span>&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group">
        <label for="composer-video-title" class="col-form-label">Title:</label>
        <input type="text" class="form-control" id="composer-video-title">
      </div>
      <div class="form-group">
        <label for="composer-video-seeker" class="col-form-label">Thumbnail:</label>
        <input type="range" class="custom-range" id="composer-video-seeker">
        <div class="composer-video-thumbnail"></div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-primary">Upload</button>
    </div>
  </form>
</div>`);
    const UploadFile = new class extends Component {
        constructor() {
            super("modal", modal_template$3);
            this.file = undefined;
            this.video = undefined;
            this.composer = undefined;
            this.main.classList.add("fade");
            this.select({
    form: ".modal-content",
    title: "#composer-video-title",
    seeker: "#composer-video-seeker",
    thumbnail: ".composer-video-thumbnail"
        });
            addEventHandler(this.form, "submit", this);
            addEventHandler(this.seeker, "change", this);
            addEventHandler(this.thumbnail, "contextmenu", this);
            addEventHandler(this.thumbnail, "dragstart", this);
            $(this.main)
                .on("shown.bs.modal", this.shown.bind(this))
                .on("hidden.bs.modal", this.hidden.bind(this));
        }
        shown() {
            this.title.select();
            const video = this.video;
            this.seeker.value = (video.currentTime = video.duration * Math.random()).toString();
        }
        hidden() {
            this.detach();
            const video = this.video;
            if (video)
                removeFromParent(video);
            this.file = undefined;
            this.video = undefined;
        }
        show() {
            this.attach(document.body);
            const video = this.video;
            const name = this.file.name;
            this.title.value = name.slice(0, name.lastIndexOf("."));
            const seeker = this.seeker;
            const duration = video.duration;
            seeker.min = "0";
            seeker.step = (duration / 20).toString();
            seeker.max = duration.toString();
            seeker.value = video.currentTime.toString();
            this.thumbnail.appendChild(video);
            $(this.main).modal("show");
        }
        handleEvent(e) {
            e.preventDefault();
            switch (e.type) {
                case "submit": {
                    const title = this.title.value;
                    const time = parseFloat(this.seeker.value);
                    this.composer.uploadFile(this.file, title, time);
                    $(this.main).modal("hide");
                    break;
        }
                case "change": {
                    this.video.currentTime = parseFloat(this.seeker.value);
                    break;
        }
        }
        }
        };

    function humanFileSize(bytes, si) {
        var thresh = si ? 1000 : 1024;
        if (Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }
        var units = si
            ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
            : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
        var u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while (Math.abs(bytes) >= thresh && u < units.length - 1);
        return bytes.toFixed(1) + ' ' + units[u];
        }

    const modal_template$4 = template(`
<div class="modal-dialog" role="document">
  <form class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Upload Failed</h5>
      <button type="button" class="close" data-dismiss="modal">
        <span>&times;</span>
      </button>
    </div>
    <div class="modal-body">
      Failed to upload file named "<span></span>".<br />
      File size should not exeed <span></span>, the provided file was <span></span>.
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">
        Dismiss
      </button>
    </div>
  </form>
</div>`);
    const UploadFail = new class extends Component {
        constructor() {
            super("modal", modal_template$4);
            this.max = 0;
            this.file = undefined;
            this.main.classList.add("fade");
        }
        show() {
            const spans = ($(this.main)
                .modal("show")
                .find(".modal-body span"));
            const file = this.file;
            spans.get(0).textContent = file.name;
            spans.get(1).textContent = humanFileSize(this.max, false);
            spans.get(2).textContent = humanFileSize(file.size, false);
        }
    };

    const modal_template$5 = template(`
<div class="modal-dialog" role="document">
  <form class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Add from URL</h5>
      <button type="button" class="close" data-dismiss="modal">
        <span>&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group">
        <label for="composer-url" class="col-form-label">URL:</label>
        <input type="text" class="form-control" id="composer-url">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-primary">Add</button>
    </div>
  </form>
</div>`);
    const AddURL = new class extends Component {
        constructor() {
            super("modal", modal_template$5);
            this.composer = undefined;
            this.provider = "";
            this.start_value = "";
            this.main.classList.add("fade");
            this.select({
    form: ".modal-content",
    url: "#composer-url"
        });
            addEventHandler(this.form, "submit", this);
            $(this.main)
                .on("shown.bs.modal", this.shown.bind(this))
                .on("hidden.bs.modal", this.hidden.bind(this));
        }
        shown() {
            this.url.focus();
        }
        hidden() {
            this.detach();
            this.start_value = "";
            this.composer = undefined;
        }
        show() {
            this.url.value = this.start_value;
            this.url.placeholder = `Please paste ${this.provider} url here`;
            this.attach(document.body);
            $(this.main).modal("show");
        }
        handleEvent(e) {
            e.preventDefault();
            if (e.type === "submit") {
                this.composer.paseURL(this.url.value);
        }
            $(this.main).modal("hide");
        }
        };

    const composer_template = template(`
<svg width="100%" height="100%">
  <g class="composer-connections--group"></g>
  <path class="composer-connection--preview"></path>
</svg>
<div class="composer-sockets--group"></div>
<div class="composer-elements--group"></div>
<input
  id="composer-sources-closed" type="checkbox"
  class="composer-sources-closed" checked />
<div class="composer-error-text">No start element, one of the elements should be marked as start.</div>
<div class="composer-sources">
  <div class="composer-sources--header">
    <i class="fas fa-video"></i> Collection <label
      class="composer-sources--close-button"
      for="composer-sources-closed"
    >&times;</label>
  </div>
  <div class="composer-sources--list"></div>
  <form class="composer-box-container">
    <label class="composer-box" for="composer-box--file">
      <i class="fas fa-upload"></i>
      <input id="composer-box--file" type="file" accept="video/*" />
    </label>
  </form>
  <div class="composer-sources--special">
    <i class="fab fa-skype" data-type="Skype"></i>
    <i class="fas fa-link" data-type="Link"></i>
    <i class="fas fa-phone" data-type="Phone"></i>
  </div>
  <div class="composer-sources--actions p-1">
    <button class="btn btn-success btn-sm btn-block" type="button" id="composer-add-video">
      <i class="fas fa-plus"></i> Add Video URL
    </button>
  </div>
</div>
<label
  class="composer-sources--open-button"
  for="composer-sources-closed"
>&raquo;</label>`);
    const composer_selectors = {
            svg: "svg",
            sources_list: ".composer-sources--list",
            closed_checkbox: ".composer-sources-closed",
            connection_preview: ".composer-connection--preview",
            connections_group: ".composer-connections--group",
            elements_group: ".composer-elements--group",
            sockets_group: ".composer-sockets--group",
            drop_box: ".composer-box",
            file_input: "#composer-box--file",
            box_container: ".composer-box-container",
            addVideo: "#composer-add-video",
            skype: ".fa-skype",
            link: ".fa-link",
            phone: ".fa-phone",
            alert: ".alert"
        };
    const drag_offset$3 = vec.create(0, 0);
    const pointer_events = [
        "pointerdown",
        "pointermove",
        "pointerup"
        ];
    class Composer extends Component {
        constructor(path_type, enableFileUpload, editElementAfterDrop, fileSizeLimit, onSelectFile, onPasteURL, getSkypeImage, getLinkImage, getPhoneImage, defaultSkypeImage, defaultLinkImage, defaultPhoneImage) {
            super("composer", composer_template);
            this.path_type = path_type;
            this.editElementAfterDrop = editElementAfterDrop;
            this.fileSizeLimit = fileSizeLimit;
            this.onSelectFile = onSelectFile;
            this.onPasteURL = onPasteURL;
            this.getSkypeImage = getSkypeImage;
            this.getLinkImage = getLinkImage;
            this.getPhoneImage = getPhoneImage;
            this.defaultSkypeImage = defaultSkypeImage;
            this.defaultLinkImage = defaultLinkImage;
            this.defaultPhoneImage = defaultPhoneImage;
            this.connections = new Collection(this, Connection, {
                onAdd(cnx) {
                    cnx.attach(this.connections_group);
        }
        });
            this.sources = new Collection(this, Source, {
                onAdd(source) {
                    source.attach(this.sources_list);
        }
        });
            this.sockets = new Collection(this, Socket, {
                onAdd(socket) {
                    socket.attach(this.sockets_group);
        }
        });
            this.elements = new Collection(this, Element, {
                onAdd(element) {
                    element.attach(this.elements_group);
                    if (this.elements.length === 1 && this.start !== element)
                        this.main.classList.add("composer-error");
        }
        });
            this.dragging = false;
            this.element = undefined;
            this.specialType = "Video";
            this.start = undefined;
            this.select(composer_selectors);
            addEventHandler(document, "keydown", this);
            addEventHandler(document, "keyup", this);
            for (const element of [this.skype, this.link, this.phone]) {
                for (const event of pointer_events) {
                    addEventHandler(element, event, this);
        }
        }
            if (enableFileUpload) {
                const drop = this.drop_box;
                $(drop).on('drag dragstart dragend dragover dragenter dragleave drop', (e) => {
                    e.preventDefault();
                    e.stopPropagation();
        });
                addEventHandler(this.file_input, "change", this);
                addEventHandler(drop, "dragenter", this);
                addEventHandler(drop, "dragleave", this);
                addEventHandler(drop, "drop", this);
        }
        else {
                removeFromParent(this.box_container);
        }
            //addEventHandler(this.addVideo, "click", this);
            if (window.COMPOSER_DEBUG) {
                console.log("Debug mode enabled.");
                const debug = document.createElement("pre");
                debug.classList.add("composer-debug");
                this.main.insertBefore(debug, this.svg);
                setInterval(() => {
                    debug.textContent = JSON.stringify(this.toJSON(), null, 1);
        }, 200);
        }
        }
        shouldNextElementBeStart() {
            return this.start === undefined;
        }
        onSpecialElementUpdate(element) {
            // @ts-ignore
            this[`get${element.type}Image`](element.question).then((src) => {
                if (src === element.image.src)
                    return;
                element.image.src = src;
        }, (error) => {
                console.warn(error);
            // @ts-ignore
                element.image.src = this[`default${element.type}Image`].src;
        });
        }
        toggleMediaLibrary(value) {
            if (typeof value === 'undefined') {
                this.setPanelDisplay(!this.closed_checkbox.checked);
        }
        else {
                this.setPanelDisplay(!!value);
        }
        }
        getTopLeft() {
            const rect = this.main.getBoundingClientRect();
            return [
                rect.left + window.scrollX,
                rect.top + window.scrollY
        ];
        }
        resize(w, h) {
            if (typeof w === "number")
                w = `${w}px`;
            if (typeof h === "number")
                h = `${h}px`;
            const style = this.main.style;
            style.width = w;
            style.height = h;
        }
        get path() { return path(this.path_type); }
        async uploadFile(file, title, time) {
            this.sources.add(Source.fromJSON(this, await this.onSelectFile(file, title, time)));
        }
        async paseURL(url) {
            this.sources.add(Source.fromJSON(this, await this.onPasteURL(url)));
        }
        failFileUpload(file) {
            UploadFail.file = file;
            UploadFail.max = this.fileSizeLimit;
            UploadFail.show();
        }
        prepareFile(file) {
            if (file.size > this.fileSizeLimit)
                return this.failFileUpload(file);
            const url = URL.createObjectURL(file);
            const video = document.createElement("video");
            console.log(url);
            video.muted = true;
            video.controls = false;
            video.pause();
            video.src = url;
            UploadFile.file = file;
            UploadFile.video = video;
            UploadFile.composer = this;
            UploadFile.show();
        }
        handlePointerEvents(e) {
            e.preventDefault();
            const target = e.target;
            const element = this.element;
            switch (e.type) {
                case "pointerdown":
                    this.setDraggingStatus(true);
                    target.setPointerCapture(e.pointerId);
                    this.specialType = target.dataset.type;
                    this.dragging = true;
                    break;
                case "pointermove":
                    if (element) {
                        vec.sub_inline_a(element.position, e.pageX - 32, e.pageY - 32, drag_offset$3);
                        element.onPositionChange();
        }
        else if (this.dragging) {
                        const [x, y] = this.getTopLeft();
                        vec.set(drag_offset$3, x, y);
                        this.setPanelDisplay(false);
                        const type = this.specialType;
                        this.element = new Element(this, type, generateUUID(), null, type, "", x, y);
        }
                    break;
                case "pointerup":
                    target.releasePointerCapture(e.pointerId);
                    this.setDraggingStatus(false);
                    if (element) {
                        this.setPanelDisplay(true);
                        if (this.editElementAfterDrop)
                            element.showEditModal();
                        this.element = undefined;
        }
                    this.dragging = false;
        }
        }
        handleDnDEvents(e) {
            switch (e.type) {
                case "drop":
                    e.preventDefault();
                    if (e.dataTransfer) {
                        for (const file of e.dataTransfer.files) {
                            if (file.type.startsWith("video/")) {
                                this.prepareFile(file);
                                break;
        }
        }
        }
                case "dragenter":
                case "dragleave":
                    boolClass(this.drop_box, "active", e.type === "dragenter");
                    break;
        }
        }
        handleEvent(e) {
            boolClass(this.main, "composer-shift", e.shiftKey);
            switch (e.type) {
                case "drop":
                case "dragenter":
                case "dragleave":
                    this.handleDnDEvents(e);
                    break;
                case "pointerdown":
                case "pointermove":
                case "pointerup":
                    this.handlePointerEvents(e);
                    break;
                case "click": {
                    AddURL.provider = e.target.dataset.provider;
                    AddURL.composer = this;
                    AddURL.show();
                    break;
        }
                case "change": {
                    const files = this.file_input.files;
                    if (files) {
                        for (const file of files) {
                            if (file.type.startsWith("video/")) {
                                this.prepareFile(file);
                                this.box_container.reset();
                                break;
        }
        }
        }
                    break;
        }
        }
        }
        setStart(element) {
            this.main.classList.remove("composer-error");
            const current = this.start;
            if (current !== element) {
                this.start = element;
                element.onStartChange(true);
                if (current === undefined)
                    return;
                current.onStartChange(false);
        }
        }
        removeStart() {
            this.start = undefined;
            if (this.elements.length !== 0) {
                this.main.classList.add("composer-error");
                $(this.alert).alert();
        }
        }
        isStart(element) {
            return this.start === element;
        }
        setDraggingStatus(status) {
            boolClass(this.main, "composer-dragging", status);
        }
        setPanelDisplay(show) {
            this.closed_checkbox.checked = !show;
        }
        toJSON() {
            return {
            Paths: this.connections.list.map(c => c.toJSON()),
            Elements: this.elements.list.map(e => e.toJSON()),
            Sources: this.sources.list.map(s => s.toJSON()),
        };
        }
        initialize(data) {
            if (data === null)
                return;
            for (const s of data.Sources)
                Source.fromJSON(this, s);
            for (const e of data.Elements)
                Element.fromJSON(this, e);
            for (const p of data.Paths)
                Connection.fromJSON(this, p);
        }
        dispose() {
            this.sources.clear();
            this.sockets.clear();
            this.elements.clear();
            this.connections.clear();
            this.detach();
        }
        }

    function createImage(src) {
        const image = new Image();
        image.src = src;
        return image;
        }
    function create({ path = "line", enableFileUpload = false, editElementAfterDrop = true, fileSizeLimit = 1024 * 1024 * 10, onSelectFile = () => Promise.reject("File upload not supported."), onPasteURL = () => Promise.reject("Video provider URLs not supported."), defaultSkypeImage, defaultLinkImage, defaultPhoneImage, getSkypeImage = () => Promise.reject("Custom Skype element image is not implemented."), getLinkImage = () => Promise.reject("Custom Link element image is not implemented."), getPhoneImage = () => Promise.reject("Custom Phone element image is not implemented.") }) {
            let composer = new Composer(path, enableFileUpload, editElementAfterDrop, fileSizeLimit, onSelectFile, onPasteURL, getSkypeImage, getLinkImage, getPhoneImage, createImage(defaultSkypeImage), createImage(defaultLinkImage), createImage(defaultPhoneImage));
            return {
                instance: composer,
                initialize({ data = null, width = "100%", height = "100%", container = document.body }) {
                composer.resize(width, height);
            composer.attach(container);
            if (data instanceof Promise)
                data.then((data) => composer.initialize(data));
            else
                composer.initialize(data);
        },
            toggleMediaLibrary(value) {
                composer.toggleMediaLibrary(value);
            },
            dispose() {
                composer.dispose();
            },
            toJSON() { return composer.toJSON(); },
            add(data) {
                return new Source(composer, data.ID, data.Title, data.Thumbnail, data.URL);
            },
            remove(uuid) {
                return composer.sources.remove(uuid);
            }
};
}

exports.create = create;
exports.uuid = generateUUID;

Object.defineProperty(exports, '__esModule', { value: true });

})));
