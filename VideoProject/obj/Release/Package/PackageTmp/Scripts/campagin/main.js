window.COMPOSER_DEBUG = false;
const composer = Composer.create(
{
	"path":"bezier"
});

composer.initialize({
  container: $(".composerElement")[0],
  data: {
    "Sources": [
      {
        "ID": "02c95277-9092-42b8-b3b9-f3a590963ca2",
        "Thumbnail": "thumbnails/02c95277-9092-42b8-b3b9-f3a590963ca2.jpg",
        "Title": "Lorum Ipsum 1 "
      },
      {
        "ID": "4aa70684-2626-4188-b2b5-d2bd8bf932e3",
        "Thumbnail": "thumbnails/4aa70684-2626-4188-b2b5-d2bd8bf932e3.jpg",
        "Title": "Lorum Ipsum 2"
      },
      {
        "ID": "e3a05a98-ab30-4e8f-a701-52ebff753aa5",
        "Thumbnail": "thumbnails/e3a05a98-ab30-4e8f-a701-52ebff753aa5.jpg",
        "Title": "Lorum Ipsum 3"
      }
    ],
    "Elements": [
      {
        "ID": "da5f9cfd-4952-438b-b00f-1d8acdab3b11",
        "SourceID": "4aa70684-2626-4188-b2b5-d2bd8bf932e3",
        "Top": "100px", "Left": "200px",
        "Name": "Some name A", "IsStart": false,
        "Question": "What is the meaning of life ?"
      },
      {
        "ID": "965d21f4-0d32-428b-adc5-ed2b7c548456",
        "SourceID": "e3a05a98-ab30-4e8f-a701-52ebff753aa5",
        "Top": "400px", "Left": "500px",
        "Name": "Some name B", "IsStart": true,
        "Question": "What is the meaning of life ?"
      }
    ],
    "Paths": [ {
      "From": "da5f9cfd-4952-438b-b00f-1d8acdab3b11",
      "To": "965d21f4-0d32-428b-adc5-ed2b7c548456",
      "Answer": "The quick brown fox jumped over the lazy dog."
    } ]
  }
});