﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoProject.Core;
using VideoProject.Model;

namespace VideoProject.Controllers
{
    public class VideoPlayController : Controller
    {
        //
        // GET: /VideoPlay/

        public ActionResult Index()
        {
            List<VideoHierarchyModel> objVideoDetailModel = new List<VideoHierarchyModel>();
            VideoHierarchyModel objVideoModel = new VideoHierarchyModel();
            objVideoModel.VideoID = 1;
            objVideoModel.IsStart = true;
            //objVideoModel.VideoURL = "https://www.youtube.com/watch?v=1Mlhnt0jMlg";
            //objVideoModel.VideoURL = "https://vimeo.com/ondemand/365film/58622266";
            objVideoModel.VideoURL = "https://vimeo.com/27221475";
            objVideoModel.QuestionID = 1;
            objVideoModel.QueDetail = "First Question";
            objVideoModel.AnswerID = 1;
            objVideoModel.AnsDetail = "First 1";
            objVideoModel.NextVideoID = 2;

            objVideoDetailModel.Add(objVideoModel);

            objVideoModel = new VideoHierarchyModel();
            objVideoModel.VideoID = 1;
            objVideoModel.IsStart = true;
            objVideoModel.VideoURL = "https://www.youtube.com/watch?v=uIxnTi4GmCo ";
            objVideoModel.QuestionID = 1;
            objVideoModel.QueDetail = "First Question";
            objVideoModel.AnswerID = 2;
            objVideoModel.AnsDetail = "Second 2";
            objVideoModel.NextVideoID = 3;
            objVideoDetailModel.Add(objVideoModel);

            objVideoModel = new VideoHierarchyModel();
            objVideoModel.VideoID = 2;
            objVideoModel.IsStart = false;
            objVideoModel.VideoURL = "https://www.youtube.com/watch?v=z2z857RSfhk ";
            objVideoModel.QuestionID = 3;
            objVideoModel.QueDetail = "Second Question";
            objVideoModel.AnswerID = 3;
            objVideoModel.AnsDetail = "Answer Second Question";
            objVideoModel.NextVideoID = 5;
            objVideoDetailModel.Add(objVideoModel);

            objVideoModel = new VideoHierarchyModel();
            objVideoModel.VideoID = 3;
            objVideoModel.IsStart = false;
            objVideoModel.VideoURL = "https://www.youtube.com/watch?v=z2z857RSfhk ";
            objVideoModel.QuestionID = -1;
            objVideoModel.QueDetail = "";
            objVideoModel.AnswerID = -1;
            objVideoModel.AnsDetail = "";
            objVideoModel.NextVideoID = -1;
            objVideoDetailModel.Add(objVideoModel);

            objVideoModel = new VideoHierarchyModel();
            objVideoModel.VideoID = 5;
            objVideoModel.IsStart = false;
            objVideoModel.VideoURL = "https://www.youtube.com/watch?v=z2z857RSfhk ";
            objVideoModel.QuestionID = -1;
            objVideoModel.QueDetail = "";
            objVideoModel.AnswerID = -1;
            objVideoModel.AnsDetail = "";
            objVideoModel.NextVideoID = -1;
            objVideoDetailModel.Add(objVideoModel);

            return View(objVideoDetailModel.ToList());
        }


        public ActionResult GetPreviewPartialPage()
        {
            return View();
        }

        public ActionResult PlayVideo(int id = 0)
        {
            VideoDetailRepository vdr = new VideoDetailRepository();
            VideoDetailModel vcm = vdr.GetUserVideoHierarchy(id);
            ViewBag.VideoDetailModel = vcm;

            return View();
        }

        public PartialViewResult PlayVideoPartialView(int id = 0)
        {
            VideoDetailRepository vdr = new VideoDetailRepository();
            VideoDetailModel vcm = vdr.GetUserVideoHierarchy(id);
            ViewBag.VideoDetailModel = vcm;

            return PartialView("~/Views/VideoPlay/_PlayVideoPartialView.cshtml");

        }

        public ActionResult TestEmbeddedCode()
        {
            return View();
        }

        public ActionResult ButtonSetDisplay(int id = 0)
        {
            ButtonSetRepository bsr = new ButtonSetRepository();
            ButtonSetModel bsm = bsr.GetButtonSetDetail(id);
            return View(bsm);
        }
    }
}
