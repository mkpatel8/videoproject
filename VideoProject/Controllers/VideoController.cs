﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoProject.Core;
using VideoProject.Model;
using System.IO;

namespace VideoProject.Controllers
{
    public class VideoController : BaseController
    {
        //
        // GET: /Video/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateCampaign()
        {
            return View();
        }

        public ActionResult CreateNewCampaign(int? id)
        {
            int VideoCampaignID = id ?? 0;
            if (VideoCampaignID > 0)
            {
                VideoDetailRepository vdr = new VideoDetailRepository();
                ViewBag.VideoCampaignModel = vdr.GetVideoCampaignModelByID(VideoCampaignID);
            }

            return View();
        }

        [HttpPost]
        public JsonResult SaveNewCampaign(VideoCampaignModel vcm)
        {
            vcm.UserId = Convert.ToInt32(HttpContext.Session.Contents["UserId"]);
            VideoDetailRepository vdr = new VideoDetailRepository();
            vcm.VideoCampaignID = vcm.NewVideoCampaignID == 0 ? vcm.VideoCampaignID : vcm.NewVideoCampaignID;
            int VideoCampaignID = vdr.SaveCampaignDetail(vcm);
            return Json(VideoCampaignID);
        }

        public ActionResult VideoCampaignList()
        {
            int UserId = Convert.ToInt32(HttpContext.Session.Contents["UserId"]);
            VideoDetailRepository vdr = new VideoDetailRepository();
            List<VideoCampaignModel> lstVideoCampaign = vdr.GetVideoCampaignList(UserId);
            return View("VideoCampaignList", lstVideoCampaign);
        }

        public PartialViewResult ChangeColorPartialView()
        {
            return PartialView("~/Views/Video/_ChangeColorPartial.cshtml");
        }

        public JsonResult SubscribeSave(string EmailId)
        {
            try
            {
                var apiKey = "5246da6b0cde2b3335025f0f335dce47-us7"; // your API key here (no, this is not a real one!)
                var listId = "792b360360"; // your list ID here
                var subscribeRequest = new
                {
                    apikey = apiKey,
                    id = listId,
                    email = new
                    {
                        email = EmailId
                    },
                    double_optin = true,
                };
                var requestJson = JsonConvert.SerializeObject(subscribeRequest);

                var responseString = CallMailChimpApi("lists/subscribe.json", requestJson);
                dynamic responseObject = JsonConvert.DeserializeObject(responseString);
                if ((responseObject.email != null) && (responseObject.euid != null))
                {
                    // successful!
                    return Json(new object[] { true, "Success" });
                }
                else
                {
                    string name = responseObject.name;
                    if (name == "List_AlreadySubscribed")
                    {
                        return Json(new object[] { false, "Email already subscribed" });
                    }
                    else
                    {
                        string error = responseObject.error;
                        return Json(new object[] { false, error });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, "Error" });
            }
        }

        private static string CallMailChimpApi(string method, string requestJson)
        {
            var endpoint = String.Format("https://{0}.api.mailchimp.com/2.0/{1}", "us7", method);
            var wc = new System.Net.WebClient();
            try
            {
                return wc.UploadString(endpoint, requestJson);
            }
            catch (System.Net.WebException we)
            {
                using (var sr = new System.IO.StreamReader(we.Response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        [HttpPost]
        public ActionResult UploadFBShareImages()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    string upload_url = string.Empty;

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = Session.SessionID + "_" + DateTime.Now.ToString("MM-dd-yyyy_hhsstt") + "_" + file.FileName;
                        }

                        string FBShareImages = Server.MapPath("~/FBShareImages/User/" + Session["UserId"] + "/");

                        if (!Directory.Exists(FBShareImages))
                        {
                            Directory.CreateDirectory(FBShareImages);
                        }

                        upload_url = ResolveServerUrl(VirtualPathUtility.ToAbsolute("~/FBShareImages/User/" + Session["UserId"] + "/" + fname), false);

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(FBShareImages, fname);

                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json(upload_url);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        public static string ResolveServerUrl(string serverUrl, bool forceHttps)
        {
            if (serverUrl.IndexOf("://") > -1)
                return serverUrl;

            string newUrl = serverUrl;
            Uri originalUri = System.Web.HttpContext.Current.Request.Url;
            newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                "://" + originalUri.Authority + newUrl;
            return newUrl;
        }
        public ActionResult VideoEdit(string id, int campaginID)
        {
            VideoDetailRepository vdr = new VideoDetailRepository();
            VideoModel vm = new VideoModel();
            if (!string.IsNullOrEmpty(id))
            {
                vm = vdr.GetVideoModelByID(id, campaginID);
            }
            string[] spliturl = vm.URL.Split('/');
            vm.VideoCampaignID = campaginID;

            if (spliturl[2].Contains("youtube"))
            {
                vm.VideoURLID = spliturl[spliturl.Length - 1].Substring(8, spliturl[spliturl.Length - 1].Length - 8);
                vm.VideoType = spliturl[2].Split('.')[1];

            }
            else
            {
                vm.VideoURLID = spliturl[spliturl.Length - 1];
                vm.VideoType = spliturl[2].Split('.')[0];

            }
            //vm.VideoElements.Add(new VideoElement { ElementName = "" });
            return View(vm);
        }
        [HttpPost]
        public int VideoEdit(List<VideoElement> model)
        {
            VideoDetailRepository vdr = new VideoDetailRepository();
            VideoModel vm = new VideoModel();
            vm.UniqueID = model.ElementAt(0).UniqueID;
            vm.VideoElements = model;
            return vdr.SaveVideoElement(vm);
        }
        public ActionResult VideoPreview(string id, int campaginID)
        {
            VideoDetailRepository vdr = new VideoDetailRepository();
            VideoModel vm = new VideoModel();
            if (!string.IsNullOrEmpty(id))
            {
                vm = vdr.GetVideoModelByID(id, campaginID);
            }
            string[] spliturl = vm.URL.Split('/');
            vm.VideoCampaignID = campaginID;
            if (spliturl[2].Contains("youtube"))
            {
                vm.VideoURLID = spliturl[spliturl.Length - 1].Substring(8, spliturl[spliturl.Length - 1].Length - 8);
                vm.VideoType = spliturl[2].Split('.')[1];

            }
            else
            {
                vm.VideoURLID = spliturl[spliturl.Length - 1];
                vm.VideoType = spliturl[2].Split('.')[0];

            }
            //  vm.VideoURLID = spliturl[spliturl.Length - 1];
            //  vm.VideoType = spliturl[2].Split('.')[0];
            //vm.VideoElements.Add(new VideoElement { ElementName = "" });
            return View("VideoPreview", vm);
        }
        public ActionResult CampaignVideoPreview(int campaginID)
        {
            VideoDetailRepository vdr = new VideoDetailRepository();
            VideoModel vm = new VideoModel();
            string id = "";
            id = vdr.GetStartVideoID(campaginID);
            if (!string.IsNullOrEmpty(id))
            {
                vm = vdr.GetVideoModelByID(id, campaginID);
            }
            string[] spliturl = vm.URL.Split('/');
            vm.VideoCampaignID = campaginID;
            if (spliturl[2].Contains("youtube"))
            {
                vm.VideoURLID = spliturl[spliturl.Length - 1].Substring(8, spliturl[spliturl.Length - 1].Length - 8);
                vm.VideoType = spliturl[2].Split('.')[1];

            }
            else
            {
                vm.VideoURLID = spliturl[spliturl.Length - 1];
                vm.VideoType = spliturl[2].Split('.')[0];

            }
            //  vm.VideoURLID = spliturl[spliturl.Length - 1];
            //  vm.VideoType = spliturl[2].Split('.')[0];
            //vm.VideoElements.Add(new VideoElement { ElementName = "" });
            return View("VideoPreview", vm);
        }

        public ActionResult DeleteVideoCampaign(int? id)
        {
            int VideoCampaignID = id ?? 0;
            if (VideoCampaignID > 0)
            {
                VideoDetailRepository objVideoDetailRepository = new VideoDetailRepository();
                objVideoDetailRepository.DeleteVideoCampaign(VideoCampaignID);
            }
            return VideoCampaignList();
        }

    }

}
