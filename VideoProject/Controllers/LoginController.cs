﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoProject.Core;
using VideoProject.Model;

namespace VideoProject.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Login()
        {
            Session.Abandon();
            LoginModel lm = new LoginModel();
            return View(lm);
        }

        [HttpPost]
        public ActionResult Login(LoginModel udm)
        {
            if (ModelState.IsValid)
            {
                UserDetailRepository udr = new UserDetailRepository();

                UserDetailModel objUDM = udr.VerifyLogin(udm.UserEmail, udm.UserPassword);
                if (objUDM != null)
                {
                    HttpContext.Session.Add("UserId", objUDM.UserId);
                    return RedirectToAction("CreateNewCampaign", "Video");
                }
            }

            TempData["InvalidUser"] = "Invalid UserName or Password";
            return View(udm);
        }

        public ActionResult HomeVideoBackup()
        {
            return View();
        }

        public ActionResult HomeVideoCurrent()
        {
            return View();
        }

        public ActionResult GetResult(int id = 1)
        {
            VideoDetailRepository vdr = new VideoDetailRepository();
            VideoDetailModel vcm = vdr.GetUserVideoHierarchy(id);
            ViewBag.VideoDetailModel = vcm;

            return PartialView("~/views/VideoPlay/GetPreviewPartialPage.cshtml");
        }

        public ActionResult SignUpSignIn()
        {
            return View();
        }

        public ActionResult Register()
        {
            UserDetailModel rm = new UserDetailModel();
            return View(rm);
        }

        [HttpPost]
        public ActionResult Register(UserDetailModel udm)
        {
            if (ModelState.IsValid)
            {
                UserDetailRepository udr = new UserDetailRepository();

                if (CommonFunctions.IsValidEmail(udm.UserEmail))
                {
                    //check if user exists
                    if (udr.Register(udm.Name, udm.UserPassword, udm.UserEmail))
                    {
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Error while registration";
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid Email Address";
                }

            }
            else
            {
                TempData["ErrorMessage"] = "Something went wrong...Try Again";
            }

            return View(udm);
        }

        public JsonResult IsEmail_Available(string UserEmail)
        {
            UserDetailRepository udr = new UserDetailRepository();

            UserDetailModel udm = udr.GetUserDetailByEmail(UserEmail);
            if (udm != null && udm.UserId > 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
