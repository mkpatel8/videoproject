﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VideoProject.Controllers
{
    public class BaseController : Controller
    {
        //
        // GET: /Base/

        protected override void OnActionExecuted(ActionExecutedContext context)
        {
            int UserId = Convert.ToInt32(context.HttpContext.Session.Contents["UserId"]);
            var rd = context.HttpContext.Request.RequestContext.RouteData;
            context.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "http://localhost:50076");
            context.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Headers", "*");
            context.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Credentials", "true");
            //TED _db = new TED();

            ////if not in DB
            //if (_db.Users.Find(ID) == null && rd.GetRequiredString("action") != "NoAccess")
            //{
            //    RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
            //    redirectTargetDictionary.Add("action", "NoAccess");
            //    redirectTargetDictionary.Add("controller", "Home");
            //    redirectTargetDictionary.Add("area", "");

            //    context.Result = new RedirectToRouteResult(redirectTargetDictionary);
            //}

            if (UserId <= 0)
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "Login");
                redirectTargetDictionary.Add("controller", "Login");
                redirectTargetDictionary.Add("area", "");

                context.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }

            base.OnActionExecuted(context);
        }
    }
}
