﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoProject.Core;
using VideoProject.Model;

namespace VideoProject.Controllers
{
    public class ButtonSetController : BaseController
    {
        #region Button Set
        public ActionResult ButtonSetList()
        {
            return View();
        }
        public PartialViewResult AddButtonSetPartialView()
        {
            return PartialView("~/Views/ButtonSet/_AddButtonSetPartial.cshtml");
        }

        [HttpGet]
        public JsonResult GetButtonSetList()
        {
            int UserID = Convert.ToInt32(HttpContext.Session.Contents["UserId"]);

            ButtonSetRepository bsr = new ButtonSetRepository();
            List<ButtonSetModel> lstButtonSet = bsr.GetButtonSetList(UserID);
            return Json(new { data = lstButtonSet }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveButtonSet(ButtonSetModel bsm)
        {
            bsm.UserID = Convert.ToInt32(HttpContext.Session.Contents["UserId"]);

            ButtonSetRepository bsr = new ButtonSetRepository();
            return Json(bsr.SaveButtonSetDetail(bsm));
        }
        #endregion
    }
}
