﻿
function MessageFadeInFadeOut(Class, Message) {

    var FadeMessage = "<div class=\"alert " + Class + " alert-dismissable\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">×</a>"
                       + Message
                       + "</div>";

    $("#dvFadeMessage").html(FadeMessage);

    $("#dvFadeMessage").fadeTo(2000, 500).slideUp(500, function () {
        $("#dvFadeMessage").slideUp(500);
    });
}
