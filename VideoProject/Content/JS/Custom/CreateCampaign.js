﻿
var idCount = 1;
var campId = 0;

function ImportVideoFromURL() {

    var x = null;
    //var videoURL = $('#txtVideoURL');
    var videoURLVal = $('#txtVideoURL').val();
    if (videoURLVal.length > 0) {
        $('#ImportVideo').modal('hide');
        $('#txtVideoURL').val('');
        $('#divImageThumbnailShow').hide();
        $('#player').show();
        var video_thumbnail = '';

        if (videoURLVal.indexOf("youtube.com") != -1 || videoURLVal.indexOf("youtu.be") != -1) {
            var iframe = $('#axPlayer');
            iframe.attr('src', videoURLVal);
            var iframe_src = iframe.attr('src');
            var youtube_video_id = iframe_src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
            if (youtube_video_id.length == 11) {
                video_thumbnail = $('<img VideoURL = "' + videoURLVal + '" src="http://img.youtube.com/vi/' + youtube_video_id + '/0.jpg">');
                $(video_thumbnail).attr('id', 'img' + idCount);
                $(video_thumbnail).addClass('imageMain');
                idCount++;
            }
        }
        else if (videoURLVal.indexOf("vimeo.com") != -1) {

            var VimeoVideoId = getVimeoVideoID(videoURLVal);

            $.ajax({
                type: 'GET',
                url: 'http://vimeo.com/api/v2/video/' + VimeoVideoId + '.json',
                jsonp: 'callback',
                dataType: 'json',
                success: function (data) {
                    video_thumbnail = $('<img VideoURL="' + videoURLVal + '" src="' + data[0].thumbnail_large + '">');
                    $(video_thumbnail).attr('id', 'img' + idCount);
                    $(video_thumbnail).addClass('imageMain');
                    idCount++;
                }
            });

            // executeFunc.Wait();
        }

        setTimeout(function () {
            $('#imgDivReplace').show();
            $('#imgDivReplace').append(video_thumbnail);
            $('#imgDivReplace img').css({ 'margin-bottom': '10px', 'margin-left': '50px' });
            //Make Images Draggable               
            DragDropLoad();
        }
            , 1000);

    }
    else {

    }
}

function ItemSpot(drag_item, spot) {
    var oldSpotitem = $(spot).find('img');
    if (oldSpotitem.length > 0) {
        $(oldSpotitem).remove();
    }
    var item = $('<img />');
    //item.attr('src', drag_item.attr('src')).attr('class', 'imageMainOnDrag').attr('id', drag_item.attr('id')).appendTo(spot).draggable({ revert: 'invalid', });

    item.attr('src', drag_item.attr('src')).attr('VideoURL', drag_item.attr('VideoURL'))
        .attr('class', 'imageMainOnDrag').appendTo(spot).draggable({ revert: 'invalid', });
}

//function to hide all childrens of particular element in hierarchy 
function hideChildrens(obj) {
    var currentLevel = $(obj).closest(".parentRow").attr("level");
    console.log(currentLevel);
    for (var i = parseInt(currentLevel) + 1; ; i++) {
        if ($("div[level=" + i + "]").length == 0) {
            break;
        }
        else {
            $("div[level=" + i + "]").find("input[type=checkbox]").prop('checked', false);
            $("div[level=" + i + "]").addClass("hide");
        }

    }
}

function getSaveVideoHierarchyArray() {
    var VideoHierarchySaveArr = [];

    $(".imageMainOnDrag").each(function () {
        var div_self = $(this).parentsUntil("div[self]").parent()[0];

        var selfID = $(div_self).attr("self");
        var parentID = $(div_self).attr("parentelement");
        var QuestionText = $($(div_self).find(".txtQue")[0]).val();
        var AnswerText = $($(div_self).find(".txtAns")[0]).val();
        var VideoUrl = $(this).attr("VideoURL");

        //alert("Self : " + selfID + "\nParentElement : " + parentID + "\nQueTxt : " 
        //+ QuestionText + "\nAnsTxt : " + AnswerText + "\nVideoURL : " + VideoUrl);

        if (typeof parentID == typeof undefined) {
            parentID = null;
        }
        if (typeof QuestionText == typeof undefined) {
            QuestionText = '';
        }
        if (typeof AnswerText == typeof undefined) {
            AnswerText = '';
        }

        var VideoHierarchySaveModel = {
            "selfID": selfID,
            "parentID": parentID,
            "QuestionText": QuestionText,
            "AnswerText": AnswerText,
            "VideoUrl": VideoUrl
        }

        VideoHierarchySaveArr.push(VideoHierarchySaveModel);
    });

    $(".txtRedirectVideoText").each(function () {
        var div_self = $(this).parentsUntil("div[self]").parent()[0];

        var selfID = $(div_self).attr("self");
        var parentID = $(div_self).attr("parentelement");
        var QuestionText = $($(div_self).find(".txtQue")[0]).val();
        var AnswerText = $($(this).parent(".row").find(".txtAns")[0]).val();
        var RedirectVideoText = $(this).val();
        var RedirectVideoType = $(this).attr("RedirectVideoType");

        if (AnswerText != '' && RedirectVideoText != '') {
            if (typeof parentID == typeof undefined) {
                parentID = null;
            }
            if (typeof QuestionText == typeof undefined) {
                QuestionText = '';
            }
            if (typeof AnswerText == typeof undefined) {
                AnswerText = '';
            }

            var VideoHierarchySaveModel = {
                "selfID": selfID,
                "parentID": parentID,
                "QuestionText": QuestionText,
                "AnswerText": AnswerText,
                "RedirectVideoText": RedirectVideoText,
                "RedirectVideoType": RedirectVideoType
            }

            VideoHierarchySaveArr.push(VideoHierarchySaveModel);
        }
    });

    return VideoHierarchySaveArr;
}

function DragDropLoad() {
    $('.imageMain').draggable({
                revert: 'invalid',
                helper: "clone",
                stack: '.imageMain',
                start: function () {
                    $('#innerImageDiv').hide();
                    $('#innerImageDiv2').show();
                    $('.subInnerImage').hide();

                    $(".dropVideo").show();
                },
                stop: function () {

                    if ($('#innerImageDiv2').find('img').length > 0) {
                        $('#innerImageDiv').hide();
                        $('#innerImageDiv2').show();
                    }
                    else {
                        $('#innerImageDiv').show();
                        $('#innerImageDiv2').hide();
                    }
                    var a = 0;
                    $(".subInnerImage_1").each(function () {

                        var ele_subInnerImage_1 = $(this);

                        var ele_subInnerImage = ele_subInnerImage_1.parent("div.row").find(".subInnerImage");

                        if (ele_subInnerImage_1.find('img').length > 0) {
                            ele_subInnerImage.hide();
                            ele_subInnerImage_1.show();
                        }
                        else {
                            ele_subInnerImage.show();
                            ele_subInnerImage_1.hide();
                        }
                    });
                }
            });

            $("#innerImageDiv2").droppable({
                accept: '.imageMain',
                activate: function () {
                    $("#innerImageDiv2").css({
                        border: "6px dashed #3CB371",
                        backgroundColor: "#D3D3D3"
                    });
                },
                deactivate: function () {
                    $("#innerImageDiv2").css({
                        border: "none"
                    });
                    $("#innerImageDiv2 .imageMainOnDrag").css("position", "");
                }
            });

            $(".dropVideo").droppable({
                accept: '.imageMain',
                activate: function () {
                    $('.dropVideo').css({
                        border: "3px dashed #3CB371",
                        backgroundColor: "#D3D3D3"
                    });
                },
                deactivate: function () {
                    $('.dropVideo').css({
                        border: "none"
                    });
                    $("#innerImageDiv2 .imageMainOnDrag,.dropVideo .imageMainOnDrag,.dropVideo .imageMainOnDrag,.dropVideo .imageMainOnDrag, .dropVideo .imageMainOnDrag").css("position", "");
                }
            });

            $("#innerImageDiv2,.dropVideo").bind('drop', function (event, ui) {
                ItemSpot(ui.draggable, this);
            });
}