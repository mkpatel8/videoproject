﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoProject.Core.Connection;
using VideoProject.Core.sp;
using VideoProject.Model;

namespace VideoProject.Core
{
    public class VideoDetailRepository
    {
        public VideoDetailModel GetUserVideoHierarchy(int VideoCampaignID)
        {
            VideoDetailModel vdm = null;

            try
            {
                vdm = new VideoDetailModel();
                vdm.lstVideoHierarchy = new List<VideoHierarchyModel>();

                SqlParameter[] p = new SqlParameter[1];

                p[0] = new SqlParameter("@VideoCampaignID", VideoCampaignID);

                DataSet ds = MyConnection.Select(SPVideoDetail.VIDEO_HIERARCHY_SELECT_BY_VIDEOCAMPAIGNID, p);

                DataRow drVideoCampaign = ds.Tables[0].Rows[0];

                vdm.vcm = new VideoCampaignModel();
                vdm.vcm.QuestionDuration = Convert.ToInt32(drVideoCampaign["QuestionDuration"]);
                vdm.vcm.QueBG = Convert.ToString(drVideoCampaign["QueBG"]);
                vdm.vcm.QueFont = Convert.ToString(drVideoCampaign["QueFont"]);
                vdm.vcm.AnsBG = Convert.ToString(drVideoCampaign["AnsBG"]);
                vdm.vcm.AnsFont = Convert.ToString(drVideoCampaign["AnsFont"]);
                vdm.vcm.AnsHoverBG = Convert.ToString(drVideoCampaign["AnsHoverBG"]);
                vdm.vcm.AnsHoverFont = Convert.ToString(drVideoCampaign["AnsHoverFont"]);


                DataTable dtVideoHierarchy = ds.Tables[1];

                foreach (DataRow drVideoHierarchy in dtVideoHierarchy.Rows)
                {
                    VideoHierarchyModel vhm = new VideoHierarchyModel();

                    vhm.VideoID = Convert.ToInt32(drVideoHierarchy["VideoID"]);
                    vhm.IsStart = Convert.ToBoolean(drVideoHierarchy["IsStart"]);
                    vhm.VideoURL = Convert.ToString(drVideoHierarchy["VideoURL"]);
                    vhm.QuestionID = Convert.ToInt32(drVideoHierarchy["QuestionID"]);
                    vhm.QueDetail = Convert.ToString(drVideoHierarchy["QueDetail"]);
                    vhm.AnswerID = Convert.ToInt32(drVideoHierarchy["AnswerID"]);
                    vhm.AnsDetail = Convert.ToString(drVideoHierarchy["AnsDetail"]);
                    vhm.NextVideoID = Convert.ToInt32(drVideoHierarchy["NextVideoID"]);
                    vhm.RedirectVideoText = Convert.ToString(drVideoHierarchy["RedirectVideoText"]);
                    vhm.RedirectVideoType = Convert.ToString(drVideoHierarchy["RedirectVideoType"]);

                    vdm.lstVideoHierarchy.Add(vhm);
                }
            }
            catch (Exception ex)
            {
                vdm = null;
                ErrorLog.ErrorLogInsert(ex);
            }

            return vdm;
        }

        public int SaveCampaignDetail(VideoCampaignModel vcm)
        {
            int VideoCampaignID = 0;

            try
            {
                SqlParameter[] p = new SqlParameter[14];

                p[0] = new SqlParameter("@videoCampaignId", vcm.VideoCampaignID);
                p[1] = new SqlParameter("@VideoCampaignName", vcm.Name);
                p[2] = new SqlParameter("@UserId", vcm.UserId);

                p[3] = new SqlParameter("@QuestionDuration", vcm.QuestionDuration);

                p[4] = new SqlParameter("@QueBG", vcm.QueBG);
                p[5] = new SqlParameter("@QueFont", vcm.QueFont);
                p[6] = new SqlParameter("@AnsBG", vcm.AnsBG);
                p[7] = new SqlParameter("@AnsFont", vcm.AnsFont);
                p[8] = new SqlParameter("@AnsHoverBG", vcm.AnsHoverBG);
                p[9] = new SqlParameter("@AnsHoverFont", vcm.AnsHoverFont);

                p[10] = new SqlParameter("@SourcesJson", vcm.SourcesJson);
                p[11] = new SqlParameter("@ElementsJson", vcm.ElementsJson);
                p[12] = new SqlParameter("@PathsJson", vcm.PathsJson);

                p[13] = new SqlParameter("@ComposerData", vcm.ComposerData);

                DataSet ds = MyConnection.Select(SPVideoDetail.CAMPAIGN_DETAIL_VIDEO_HIERARCHY_SAVE, p);

                VideoCampaignID = Convert.ToInt32(ds.Tables[0].Rows[0]["VideoCampaignID"]);
            }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogInsert(ex);
            }

            return VideoCampaignID;
        }

        public DataTable SelectCampaignDetail(int UserId, string CampaignName)
        {
            DataTable dtCampaignDetail = null;

            try
            {
                SqlParameter[] p = new SqlParameter[2];
                p[0] = new SqlParameter("@VideoCampaignName", CampaignName);
                p[1] = new SqlParameter("@UserId", UserId);

                DataSet ds = MyConnection.Select(SPVideoDetail.VIDEO_CAMPAIGN_SELECT_BY_NAME_USER, p);

                dtCampaignDetail = ds.Tables[0];
            }
            catch (Exception ex)
            {
                dtCampaignDetail = null;
            }

            return dtCampaignDetail;
        }

        public List<VideoCampaignModel> GetVideoCampaignList(int UserId)
        {
            List<VideoCampaignModel> lstVideoCampaign;

            try
            {
                lstVideoCampaign = new List<VideoCampaignModel>();

                SqlParameter[] p = new SqlParameter[1];

                p[0] = new SqlParameter("@UserId", UserId);

                DataSet ds = MyConnection.Select(SPVideoDetail.VIDEO_CAMPAIGN_LIST_SELECT_BY_USER, p);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    VideoCampaignModel vcm = new VideoCampaignModel();

                    vcm.VideoCampaignID = Convert.ToInt32(dr["VideoCampaignID"]);
                    vcm.Name = Convert.ToString(dr["Name"]);
                    vcm.CreatedDate = Convert.ToString(dr["CreatedDate"]);
                    vcm.URL = Convert.ToString(dr["URL"]);
                    lstVideoCampaign.Add(vcm);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogInsert(ex);
                lstVideoCampaign = null;
            }

            return lstVideoCampaign;
        }

        public VideoCampaignModel GetVideoCampaignModelByID(int VideoCampaignID)
        {
            VideoCampaignModel vcm = null;

            try
            {
                SqlParameter[] p = new SqlParameter[1];
                p[0] = new SqlParameter("@VideoCampaignID", VideoCampaignID);
                DataSet ds = MyConnection.Select(SPVideoDetail.VIDEO_CAMPAIGN_SELECT_BY_VIDEOCAMPAIGNID, p);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    vcm = new VideoCampaignModel();

                    DataRow drVideoCampaign = ds.Tables[0].Rows[0];

                    vcm.VideoCampaignID = Convert.ToInt32(drVideoCampaign["VideoCampaignID"]);
                    vcm.Name = Convert.ToString(drVideoCampaign["Name"]);
                    vcm.UserId = Convert.ToInt32(drVideoCampaign["UserId"]);
                    vcm.QuestionDuration = Convert.ToInt32(drVideoCampaign["QuestionDuration"]);
                    vcm.CreatedDate = Convert.ToString(drVideoCampaign["CreatedDate"]);
                    vcm.QueBG = Convert.ToString(drVideoCampaign["QueBG"]);
                    vcm.QueFont = Convert.ToString(drVideoCampaign["QueFont"]);
                    vcm.AnsBG = Convert.ToString(drVideoCampaign["AnsBG"]);
                    vcm.AnsFont = Convert.ToString(drVideoCampaign["AnsFont"]);
                    vcm.AnsHoverBG = Convert.ToString(drVideoCampaign["AnsHoverBG"]);
                    vcm.AnsHoverFont = Convert.ToString(drVideoCampaign["AnsHoverFont"]);
                    vcm.ComposerData = Convert.ToString(drVideoCampaign["ComposerData"]);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogInsert(ex);
            }

            return vcm;
        }
        public VideoModel GetVideoModelByID(string UniqueID, int VideoCampaignID)
        {
            VideoModel vm = null;

            try
            {
                SqlParameter[] p = new SqlParameter[2];
                p[0] = new SqlParameter("@VideoCampaignID", VideoCampaignID);
                p[1] = new SqlParameter("@UniqueID", UniqueID);
                DataSet ds = MyConnection.Select(SPVideoDetail.VIDEO_SELECT_BY_UNIQUEID, p);
                vm = new VideoModel();
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow drVideoCampaign = ds.Tables[0].Rows[0];
                    vm.VideoCampaignID = Convert.ToInt32(drVideoCampaign["VideoCampaignID"]);
                    vm.URL = Convert.ToString(drVideoCampaign["URL"]);
                    vm.VideoID = Convert.ToInt32(drVideoCampaign["VideoID"]);
                    vm.UniqueID = Convert.ToString(drVideoCampaign["UniqueID"]);
                }
                if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {
                    for (int rowcount = 0; rowcount < ds.Tables[1].Rows.Count; rowcount++)
                    {
                        DataRow drVideoElement = ds.Tables[1].Rows[rowcount];
                        VideoElement vme = new VideoElement();
                        vme.ElementID = Convert.ToInt32(drVideoElement["ElementID"]);
                        vme.Action = Convert.ToString(drVideoElement["Action"]);
                        vme.ActionItem = Convert.ToString(drVideoElement["ActionItem"]);
                        vme.ElementHeight = Convert.ToInt32(drVideoElement["ElementHeight"]);
                        vme.ElementName = Convert.ToString(drVideoElement["ElementName"]);
                        vme.ElementPositionX = Convert.ToInt32(drVideoElement["ElementPositionX"]);
                        vme.ElementPositionY = Convert.ToInt32(drVideoElement["ElementPositionY"]);
                        vme.ElementType = Convert.ToInt32(drVideoElement["ElementType"]);
                        vme.VideoID = Convert.ToInt32(drVideoElement["VideoID"]);
                        vme.ElementWidth = Convert.ToInt32(drVideoElement["ElementWidth"]);
                        vme.pauseWhileShow = Convert.ToBoolean(drVideoElement["pauseWhileShow"]);
                        vme.Timein = Convert.ToDouble(drVideoElement["Timein"]);
                        vme.TimeOut = Convert.ToDouble(drVideoElement["TimeOut"]);
                        vme.FontColor = Convert.ToString(drVideoElement["FontColor"]);
                        vme.FontSize = Convert.ToInt32(drVideoElement["FontSize"]);
                        vme.UniqueID = Convert.ToString(drVideoElement["UniqueID"]);
                        vme.BackColor = Convert.ToString(drVideoElement["BackColor"]);

                        vm.VideoElements.Add(vme);
                    }
                }
                if (ds.Tables.Count > 1 && ds.Tables[2].Rows.Count > 0)
                {
                    for (int rowcount = 0; rowcount < ds.Tables[2].Rows.Count; rowcount++)
                    {
                        DataRow drVideoItem = ds.Tables[2].Rows[rowcount];
                        selectList videoItem = new selectList();
                        videoItem.Text = Convert.ToString(drVideoItem["VideoID"]);
                        videoItem.Value = Convert.ToString(drVideoItem["UniqueID"]);
                        videoItem.Type = Convert.ToString(drVideoItem["Type"]);
                        vm.selectLists.Add(videoItem);
                    }
                }

                    }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogInsert(ex);
            }

            return vm;
        }
        public int SaveVideoElement(VideoModel vm)
        {
            CommonFunctions.ToDataTable(vm.VideoElements);
            try
            {
                SqlParameter[] p = new SqlParameter[1];
                p[0] = new SqlParameter("@tableSave", CommonFunctions.ToDataTable(vm.VideoElements));
                MyConnection.Insert(SPVideoDetail.VIDEO_ELEMENT_SAVE, p);
                return 1;
            }
            catch (Exception ex)
            {

                ErrorLog.ErrorLogInsert(ex);
                return 0;
            }

        }
        public string GetStartVideoID(int VideoCampaignID)
        {
            
            try
            {
                SqlParameter[] p = new SqlParameter[1];
                p[0] = new SqlParameter("@VideoCampaignID", VideoCampaignID);
                DataSet ds = MyConnection.Select("sp_videomodel_select_startvideo_by_campaign_id", p);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0][0].ToString();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogInsert(ex);
            }

            return "";
        }

        public void DeleteVideoCampaign(int VideoCampaignID)
        {
            List<SqlParameter> liSqlParameters = new List<SqlParameter>();
            liSqlParameters.Add(new SqlParameter("@VideoCampaignID", VideoCampaignID));
            MyConnection.Delete(SPVideoDetail.VIDEO_CAMPAIGN_DELETE, liSqlParameters.ToArray());
        }
    }
}
