USE [master]
GO
/****** Object:  Database [VideoSoftware]    Script Date: 8/19/2018 4:11:22 PM ******/
CREATE DATABASE [VideoSoftware]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VideoSoftware', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\VideoSoftware.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VideoSoftware_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\VideoSoftware_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VideoSoftware] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VideoSoftware].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VideoSoftware] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VideoSoftware] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VideoSoftware] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VideoSoftware] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VideoSoftware] SET ARITHABORT OFF 
GO
ALTER DATABASE [VideoSoftware] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [VideoSoftware] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VideoSoftware] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VideoSoftware] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VideoSoftware] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VideoSoftware] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VideoSoftware] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VideoSoftware] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VideoSoftware] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VideoSoftware] SET  ENABLE_BROKER 
GO
ALTER DATABASE [VideoSoftware] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VideoSoftware] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VideoSoftware] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VideoSoftware] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VideoSoftware] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VideoSoftware] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VideoSoftware] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VideoSoftware] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [VideoSoftware] SET  MULTI_USER 
GO
ALTER DATABASE [VideoSoftware] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VideoSoftware] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VideoSoftware] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VideoSoftware] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [VideoSoftware] SET DELAYED_DURABILITY = DISABLED 
GO
USE [VideoSoftware]
GO
/****** Object:  UserDefinedTableType [dbo].[SetCampaignDetail]    Script Date: 8/19/2018 4:11:22 PM ******/
CREATE TYPE [dbo].[SetCampaignDetail] AS TABLE(
	[selfID] [int] NULL,
	[parentID] [int] NULL,
	[QuestionText] [nvarchar](max) NULL,
	[AnswerText] [nvarchar](max) NULL,
	[VideoUrl] [varchar](max) NULL,
	[VideoId] [int] NULL,
	[QuestionId] [int] NULL,
	[AnswerId] [int] NULL,
	[RedirectVideoText] [varchar](max) NULL,
	[RedirectVideoType] [varchar](50) NULL
)
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Answer](
	[AnswerID] [int] IDENTITY(1,1) NOT NULL,
	[AnsDetail] [nvarchar](max) NOT NULL,
	[QuestionID] [int] NOT NULL,
	[VideoID] [int] NULL,
	[RedirectVideoText] [varchar](max) NULL,
	[RedirectVideoType] [varchar](50) NULL,
 CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Button]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Button](
	[ButtonID] [int] IDENTITY(1,1) NOT NULL,
	[ButtonText] [varchar](max) NOT NULL,
	[ButtonBackgroundColor] [varchar](50) NOT NULL,
	[ButtonFontColor] [varchar](50) NOT NULL,
	[ButtonHoverBackgroundColor] [varchar](50) NOT NULL,
	[ButtonHoverFontColor] [varchar](50) NOT NULL,
	[ButtonLink] [varchar](max) NOT NULL,
	[ButtonSetID] [int] NOT NULL,
 CONSTRAINT [PK_Button] PRIMARY KEY CLUSTERED 
(
	[ButtonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ButtonSet]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ButtonSet](
	[ButtonSetID] [int] IDENTITY(1,1) NOT NULL,
	[ButtonSetName] [varchar](max) NOT NULL,
	[UserID] [int] NOT NULL,
	[IsDisplayHorizontal] [bit] NOT NULL,
 CONSTRAINT [PK_ButtonSet] PRIMARY KEY CLUSTERED 
(
	[ButtonSetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Question]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[QuestionID] [int] IDENTITY(1,1) NOT NULL,
	[QueDetail] [nvarchar](max) NOT NULL,
	[VideoID] [int] NOT NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserDetail]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserDetail](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[UserPassword] [varchar](50) NOT NULL,
	[UserEmail] [varchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRole](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Video]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Video](
	[VideoID] [int] IDENTITY(1,1) NOT NULL,
	[URL] [varchar](max) NOT NULL,
	[VideoCampaignID] [int] NOT NULL,
	[IsStart] [bit] NOT NULL,
 CONSTRAINT [PK_Video] PRIMARY KEY CLUSTERED 
(
	[VideoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoCampaign]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoCampaign](
	[VideoCampaignID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[UserId] [int] NOT NULL,
	[QuestionDuration] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[QueBG] [varchar](50) NULL,
	[QueFont] [varchar](50) NULL,
	[AnsBG] [varchar](50) NULL,
	[AnsFont] [varchar](50) NULL,
	[AnsHoverFont] [varchar](50) NULL,
	[AnsHoverBG] [varchar](50) NULL,
 CONSTRAINT [PK_VideoCampaign] PRIMARY KEY CLUSTERED 
(
	[VideoCampaignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Answer] ON 

GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (1, N'Ans 1', 1, 2, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (2, N'Ans 1', 2, 4, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (3, N'Ans 2', 2, NULL, N'www.fb.com', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (4, N'Ans 1', 3, 6, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (5, N'Ans 2', 3, 7, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (6, N'Link', 3, NULL, N'facebook.com', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (7, N'L1 A1', 4, 8, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (8, N'Contact', 4, NULL, N'1234', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (9, N'Skype', 5, NULL, N'test.1234', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (10, N'Ans 1', 6, 10, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (11, N'Ans 2', 6, NULL, N'121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (12, N'Ans 1', 7, 12, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (13, N'Call me 123', 7, NULL, N'123', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (14, N'bookings', 8, 14, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (15, N'quality', 8, 15, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (16, N'prices', 8, 16, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (45, N'bookings', 9, 46, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (46, N'quality', 9, 47, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (47, N'prices', 9, 48, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (48, N'Book now', 9, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (49, N'1', 10, 50, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (50, N'2', 10, 51, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (51, N'3', 10, 52, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (52, N'4', 10, NULL, N'www.bugslife.com.au', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (53, N'1', 11, 54, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (54, N'2', 11, 55, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (55, N'3', 11, 56, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (56, N'4', 11, NULL, N'http://www.bugslife.com.au', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (57, N'1', 12, 58, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (58, N'2', 12, 59, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (59, N'3', 12, 60, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (60, N'4', 12, NULL, N'www.bugslife.com.au', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (61, N'1', 13, 62, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (62, N'2', 13, 63, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (63, N'3', 13, 64, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (64, N'4', 13, NULL, N'www.bugslife.com.au', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (65, N'1', 15, 70, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (66, N'2', 15, 71, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (67, N'3', 15, 72, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (68, N'4', 15, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (69, N'1', 16, 74, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (70, N'2', 16, 75, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (71, N'3', 16, 76, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (72, N'4', 16, NULL, N'paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (73, N'1', 17, 78, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (74, N'2', 17, 79, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (75, N'3', 17, 80, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (76, N'4', 17, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (77, N'sometimes', 18, 82, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (78, N'always', 18, 83, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (79, N'never', 18, 84, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (80, N'phone a friend', 18, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (81, N'yes ', 19, 86, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (82, N'no', 19, 87, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (83, N'don''t know', 19, 88, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (84, N'call', 19, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (168, N'spiders', 52, 155, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (169, N'ants', 52, 156, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (170, N'cockroaches', 52, 157, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (171, N'spiders', 53, 159, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (172, N'ants', 53, 160, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (173, N'cockroaches', 53, 161, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (174, N'click here', 54, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (175, N'spiders', 57, 163, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (176, N'ants', 57, 164, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (177, N'cockroaches', 57, 165, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (178, N'click here', 59, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (179, N'spiders', 61, 167, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (180, N'ants', 61, 168, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (181, N'cockroaches', 61, 169, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (182, N'click here', 64, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (183, N'spiders', 65, 171, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (184, N'ants', 65, 172, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (185, N'cockroaches', 65, 173, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (186, N'click here', 68, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (187, N'spiders', 69, 175, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (188, N'ants', 69, 176, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (189, N'cockroaches', 69, 177, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (190, N'click here', 72, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (191, N'click here', 73, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (192, N'watch more', 74, 180, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (193, N'click here', 75, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (194, N'watch more', 76, 182, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (195, N'to book', 76, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (196, N'click here', 77, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (197, N'watch more', 78, 184, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (198, N'to book', 78, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (199, N'click here', 79, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (201, N'watch more', 80, 188, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (202, N'Click here', 83, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (203, N'watch more', 84, 192, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (204, N'Click here', 87, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (205, N'spiders', 88, 194, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (206, N'ants', 88, 195, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (207, N'cockraoches', 88, 196, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (208, N'book now', 88, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (209, N'click', 89, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (210, N'click here', 90, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (211, N'click now', 91, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (212, N'spiders', 92, 198, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (213, N'ants', 92, 199, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (214, N'cockraoches', 92, 200, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (215, N'book now', 92, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (216, N'watch more', 93, 201, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (217, N'click', 93, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (218, N'click here', 94, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (219, N'click now', 95, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (220, N'spiders', 96, 203, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (221, N'ants', 96, 204, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (222, N'cockraoches', 96, 205, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (223, N'book now', 96, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (224, N'watch more', 97, 206, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (225, N'ask question', 97, 207, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (226, N'click', 97, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (227, N'click here', 98, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (228, N'click now', 99, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (235, N'1', 102, 217, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (236, N'2', 102, 218, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (237, N'3', 102, 219, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (238, N'4', 102, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (239, N'A1', 103, 221, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (240, N'A2', 103, 222, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (241, N'Echo call', 103, NULL, N'echo123', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (242, N'A1', 104, 223, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (243, N'A1', 105, 224, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (262, N'A1', 115, 241, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (263, N'FB', 115, NULL, N'fb.com', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (264, N'skype', 115, NULL, N'echo123', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (268, N'Test Ans', 117, 245, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (269, N'facebpok', 117, NULL, N'FB', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (270, N'Skype', 117, NULL, N'echo123', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (271, N'1', 118, 247, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (272, N'2', 118, 248, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (273, N'3', 118, 249, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (274, N'1', 119, 251, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (275, N'2', 119, 252, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (276, N'3', 119, 253, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (277, N'11', 120, 255, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (278, N'22', 120, 256, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (279, N'33', 120, 257, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (280, N'11', 121, 259, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (281, N'22', 121, 260, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (282, N'33', 121, 261, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (283, N'1', 122, 263, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (284, N'2', 122, 264, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (285, N'3', 122, 265, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (286, N'Contact us here', 122, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (287, N'call us', 122, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (288, N'skype us', 122, NULL, N'paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (289, N'1', 123, 267, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (290, N'2', 123, 268, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (291, N'3', 123, 269, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (292, N'Contact us here', 123, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (293, N'call us', 123, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (294, N'skype us', 123, NULL, N'paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (295, N'1', 124, 271, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (296, N'2', 124, 272, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (297, N'3', 124, 273, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (298, N'Contact us here', 124, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (299, N'call us', 124, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (300, N'skype us', 124, NULL, N'paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (301, N'1', 125, 275, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (302, N'2', 125, 276, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (303, N'3', 125, 277, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (304, N'1', 126, 279, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (305, N'2', 126, 280, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (306, N'3', 126, 281, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (307, N'4', 126, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (308, N'5', 126, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (309, N'6', 126, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (310, N'Ans 1', 127, 283, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (311, N'Ans 2', 127, NULL, N'www.facebook.com', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (312, N'1', 128, 285, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (313, N'2', 128, 286, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (314, N'3', 128, 287, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (315, N'4', 128, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (316, N'5', 128, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (317, N'6', 128, NULL, N'paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (318, N'1', 129, 289, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (319, N'2', 129, 290, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (320, N'3', 129, 291, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (321, N'for more info', 129, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (322, N'call', 129, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (323, N'skype', 129, NULL, N'paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (324, N'Subscribe', 129, NULL, N'www.bugslife.com.au', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (325, N'1', 130, 293, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (326, N'2', 130, 294, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (327, N'3', 130, 295, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (328, N'for more info', 130, NULL, N'www.bugslife.com.au', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (329, N'call', 130, NULL, N'0404378121', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (330, N'skype', 130, NULL, N'paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (331, N'Subscribe', 130, NULL, N'www.bugslife.com.au', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (332, N'Answer', 131, 297, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (333, N'Link', 131, NULL, N'https://vimeo.com/', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (334, N'Ans 1', 132, 299, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (335, N'Get it on Amazon', 133, NULL, N'https://www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (336, N'Get it on Amazon', 134, NULL, N'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=retropia+leee+john', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (337, N'Get it on Amazon', 135, NULL, N'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (338, N'Get it on Amazon', 136, NULL, N'https://goo.gl/7b5mv1', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (339, N'Get it on Amazon', 137, NULL, N'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (340, N'Get it on Amazon', 138, NULL, N'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (341, N'Get it on Amazon', 139, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (342, N'Get it on Amazon', 140, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (343, N'Get it on Amazon', 141, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (344, N'Post a Review', 141, NULL, N'http://52.15.46.43/VideoSoftware/VideoPlay/PlayVideo/79', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (345, N'Get it on Amazon', 142, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (346, N'Post a Review', 142, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (347, N'Get it on Amazon UK', 143, NULL, N'https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (348, N'Get it on Amazon', 143, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (349, N'Post a Review', 143, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (350, N'Get it on Amazon UK', 144, NULL, N'https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (351, N'Get it on Amazon', 144, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (352, N'Post a Review', 144, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (353, N'Get it on Amazon UK', 145, NULL, N'https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (354, N'Get it on Amazon', 145, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (355, N'Post a Review', 145, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (356, N'Get it on Amazon UK', 146, NULL, N'www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (357, N'Get it on Amazon', 146, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (358, N'Post a Review', 146, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (359, N'Get it Now at Amazon UK', 147, NULL, N'www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (360, N'Get it Now at Amazon.com', 147, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (361, N'Post a Review', 147, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (362, N'Get it Now at Amazon UK', 148, NULL, N'www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (363, N'Get it Now at Amazon.com', 148, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (364, N'Post a Review', 148, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (365, N'Get it Now at Amazon UK', 149, NULL, N'www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (366, N'Get it Now at Amazon.com', 149, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (367, N'Post a Review', 149, NULL, N'www.amazon.com/Retropia-Imagination-Leee-John/dp/B06XSN1YST/ref=sr_1_2?ie=UTF8&qid=1511867664&sr=8-2&keywords=leee+john+retropia', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (368, N'leeejohn.com', 150, NULL, N'http://leeejohn.com/gigs.html', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (369, N'229 The Venue', 150, NULL, N'https://ishl.starrezhousing.com/StarRezPortalEvents/Default.aspx?Params=L9ezxPcQnQvPF6HX%2bWhPkPJMQLof0AJms%2b%2f7bqyGkkQ%3d', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (370, N'leeejohn.com', 151, NULL, N'http://www.leeejohn.com/gigs.html', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (371, N'229 The Venue', 151, NULL, N'https://www.ishl.starrezhousing.com/StarRezPortalEvents/Default.aspx?Params=L9ezxPcQnQvPF6HX%2bWhPkPJMQLof0AJms%2b%2f7bqyGkkQ%3d', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (372, N'leeejohn.com', 152, NULL, N'www.leeejohn.com/gigs.html', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (373, N'229 The Venue', 152, NULL, N'www.ishl.starrezhousing.com/StarRezPortalEvents/Default.aspx?Params=L9ezxPcQnQvPF6HX%2bWhPkPJMQLof0AJms%2b%2f7bqyGkkQ%3d', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (374, N'leeejohn.com', 153, NULL, N'www.leeejohn.com/gigs.html', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (375, N'229thevenue.com', 153, NULL, N'ishl.starrezhousing.com/StarRezPortalEvents/Default.aspx?Params=L9ezxPcQnQvPF6HX%2bWhPkPJMQLof0AJms%2b%2f7bqyGkkQ%3d', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (376, N'229thevenue.com', 154, NULL, N'ishl.starrezhousing.com/StarRezPortalEvents/Default.aspx?Params=L9ezxPcQnQvPF6HX%2bWhPkPJMQLof0AJms%2b%2f7bqyGkkQ%3d', N'Link')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (377, N'leeejohn.com', 154, NULL, N'www.leeejohn.com/gigs.html', N'Subscribe')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (378, N'Buy The Album', 154, NULL, N'www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=leee+john+retropia+', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (379, N'Click To Call', 155, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (380, N'Skype Me', 155, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (381, N'Post A Review On This Business', 155, NULL, N'https://goo.gl/TnpwXJ', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (382, N'Click To Call', 156, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (383, N'Skype Me', 156, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (384, N'Post A Review On This Business', 156, NULL, N'https://goo.gl/PV1Nmq', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (385, N'Click To Call', 157, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (386, N'Skype Me', 157, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (387, N'Post A Review On This Business', 157, NULL, N'http://search.google.com/local/writereview?placeid=ChIJc1LjOBBYzSsRF3Nwnoevhq4', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (388, N'Click To Call', 158, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (389, N'Skype Me', 158, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (390, N'Post A Review On This Business', 158, NULL, N'https://www.google.com.au/search?ei=W3U7W7P8EMSz8QXprq6gDQ&q=bugs+life+pest+control+&oq=bugs+life+pest+control+&gs_l=psy-ab.3...3416.3416.0.3626.1.1.0.0.0.0.0.0..0.0....0...1c.1.64.psy-ab..1.0.0....0.ZU829bsu9rI', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (391, N'Click To Call', 159, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (392, N'Skype Me', 159, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (393, N'Post A Review On This Business', 159, NULL, N'http://search.google.com/local/writereview?placeid=ChIJc1LjOBBYzSsRF3Nwnoevhq4', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (394, N'Click To Call', 160, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (395, N'Skype Me', 160, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (396, N'Post A Review On This Business', 160, NULL, N'search.google.com/local/writereview?placeid=ChIJc1LjOBBYzSsRF3Nwnoevhq4', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (397, N'Click Here To Find Out', 161, 330, NULL, NULL)
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (398, N'Click To Call', 161, NULL, N'0416081923', N'Contact')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (399, N'Skype Me', 161, NULL, N'Paulio165', N'Skype')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (400, N'Post A Review On This Business', 161, NULL, N'search.google.com/local/writereview?placeid=ChIJc1LjOBBYzSsRF3Nwnoevhq4', N'PostReview')
GO
INSERT [dbo].[Answer] ([AnswerID], [AnsDetail], [QuestionID], [VideoID], [RedirectVideoText], [RedirectVideoType]) VALUES (401, N'I want It', 162, NULL, N'www.magicpageplugin.com', N'Link')
GO
SET IDENTITY_INSERT [dbo].[Answer] OFF
GO
SET IDENTITY_INSERT [dbo].[Button] ON 

GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (4, N'nsmdnms', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'scsmns', 4)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (5, N'dfdfsfasfassdasdasd', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'asdsad', 4)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (6, N'Facebook', N'#e98d8d', N'#69ff1d', N'#d3b80b', N'#ad8f8f', N'facebook.com', 5)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (7, N'Twitter', N'#983131', N'#0f3157', N'#000000', N'#a91111', N'twitter.com', 5)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (8, N'msdnsam', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'snmdnsa', 6)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (9, N'smdnsam,', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'msnmsn', 6)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (12, N'mnsdm', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'sdnm', 8)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (13, N'mscnms', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'smds', 8)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (14, N'nsdmd', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'smdn', 9)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (15, N'dsms', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'msndmn', 9)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (16, N'mndmsan', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'msndmsan', 10)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (17, N'dmksamnd', N'#b20404', N'#1d89ff', N'#1d89ff', N'#ffffff', N'sdmsnd', 10)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (20, N's ds', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'smdmsa', 12)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (21, N',smd,s', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'msndms', 12)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (22, N'ksamds,a', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'skadsa', 13)
GO
INSERT [dbo].[Button] ([ButtonID], [ButtonText], [ButtonBackgroundColor], [ButtonFontColor], [ButtonHoverBackgroundColor], [ButtonHoverFontColor], [ButtonLink], [ButtonSetID]) VALUES (23, N'msdnsam', N'#ffffff', N'#1d89ff', N'#1d89ff', N'#ffffff', N'mnsdmsa', 13)
GO
SET IDENTITY_INSERT [dbo].[Button] OFF
GO
SET IDENTITY_INSERT [dbo].[ButtonSet] ON 

GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (4, N'Sjdhjs', 2, 0)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (5, N'Social SItes', 2, 1)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (6, N'djasbjas', 2, 1)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (7, N'asjhnAJS', 2, 1)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (8, N'asnamn', 2, 1)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (9, N'sdjksad', 2, 1)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (10, N'asnma', 2, 1)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (11, N'New', 2, 1)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (12, N'dsd,', 2, 0)
GO
INSERT [dbo].[ButtonSet] ([ButtonSetID], [ButtonSetName], [UserID], [IsDisplayHorizontal]) VALUES (13, N',msdnmsa', 2, 1)
GO
SET IDENTITY_INSERT [dbo].[ButtonSet] OFF
GO
SET IDENTITY_INSERT [dbo].[Question] ON 

GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (1, N'Test Question', 1)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (2, N'Test Question', 3)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (3, N'First Question ?', 5)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (4, N'L 1 Q 1 ?', 7)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (5, N'L2 Q1 ?', 8)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (6, N'Test', 9)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (7, N'Question 1', 11)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (8, N'Would you like to know more about our hog roasts', 13)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (9, N'what do you want to know', 45)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (10, N'this is a test', 49)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (11, N'this is a test', 53)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (12, N'this is a test', 57)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (13, N'test', 61)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (14, N'test', 65)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (15, N'test', 69)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (16, N'test', 73)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (17, N'1', 77)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (18, N'are you a dick', 81)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (19, N'test', 85)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (52, N'what pest would you like to know about? ', 154)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (53, N'what pest would you like to know about? ', 158)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (54, N'to book', 159)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (55, N'to call', 160)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (56, N'to skype', 161)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (57, N'what pest would you like to know about? ', 162)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (58, N'to book', 163)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (59, N'to call', 164)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (60, N'to skype', 165)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (61, N'what pest would you like to know about? ', 166)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (62, N'to book', 167)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (63, N'to call', 168)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (64, N'to skype', 169)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (65, N'what pest would you like to know about? ', 170)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (66, N'to book', 171)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (67, N'to call', 172)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (68, N'to skype', 173)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (69, N'what pest would you like to know about? ', 174)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (70, N'to book', 175)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (71, N'to call', 176)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (72, N'to skype', 177)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (73, N'what pests', 178)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (74, N'what pests', 179)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (75, N'to book', 180)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (76, N'more infor', 181)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (77, N'to book ', 182)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (78, N'more infor', 183)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (79, N'to book ', 184)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (80, N'what bugs', 185)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (81, N'to call', 186)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (82, N'to skype', 187)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (83, N'to book', 188)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (84, N'what bugs', 189)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (85, N'to call', 190)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (86, N'to skype', 191)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (87, N'to book', 192)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (88, N'what pests', 193)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (89, N'to call', 194)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (90, N'to skype', 195)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (91, N'to book', 196)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (92, N'what pests', 197)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (93, N'to call', 198)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (94, N'to skype', 199)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (95, N'to book', 200)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (96, N'what pests', 202)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (97, N'to call', 203)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (98, N'to skype', 204)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (99, N'to book', 205)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (102, N'tester', 216)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (103, N'Test Question ?', 220)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (104, N'Question 2 ', 221)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (105, N'Question 3', 222)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (106, N'Test Question ?', 225)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (107, N'Question 2 ', 226)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (108, N'Question 3', 227)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (109, N'Test Question ?', 230)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (110, N'Question 2 ', 231)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (111, N'Question 3', 232)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (112, N'Test Question ?', 235)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (113, N'Question 2 ', 236)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (114, N'Question 3', 237)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (115, N'Test Q1', 240)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (117, N'Test 1234', 244)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (118, N'what is it', 246)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (119, N'what is it', 250)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (120, N'what is it 2', 254)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (121, N'what is it 2', 258)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (122, N'what is it', 262)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (123, N'what is it', 266)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (124, N'what is it', 270)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (125, N'what is it', 274)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (126, N'what is it', 278)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (127, N'This is my question 1', 282)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (128, N'what is it', 284)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (129, N'what is it', 288)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (130, N'what is it', 292)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (131, N'Question ?', 296)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (132, N'Test 1', 298)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (133, N'Get The New Album From Leee John and Imagination ', 300)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (134, N'Get The New Album From Leee John and Imagination ', 301)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (135, N'Get The New Album From Leee John and Imagination ', 302)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (136, N'Get The New Album From Leee John and Imagination ', 303)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (137, N'Get The New Album From Leee John and Imagination ', 304)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (138, N'Get The New Album From Leee John and Imagination ', 305)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (139, N'Get The New Album From Leee John and Imagination ', 306)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (140, N'Get The New Album From Leee John and Imagination ', 307)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (141, N'Get The New Album From Leee John and Imagination ', 308)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (142, N'Get The New Album From Leee John and Imagination ', 309)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (143, N'Get The New Album From Leee John and Imagination ', 310)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (144, N'Get The New Album From Leee John and Imagination ', 311)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (145, N'Get The New Album From Leee John and Imagination ', 312)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (146, N'Get The New Album From Leee John and Imagination ', 313)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (147, N'Out Now!', 314)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (148, N'Out Now!', 315)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (149, N'Out Now!', 316)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (150, N'Book Now!', 317)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (151, N'Book Now!', 318)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (152, N'Book Now!', 319)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (153, N'Book Now!', 320)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (154, N'Book Now!', 321)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (155, N'Is Mike Martins Software Cool', 323)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (156, N'Is Mike Martins Software Cool', 324)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (157, N'Is Mike Martins Software Cool', 325)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (158, N'Is Mike Martins Software Cool', 326)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (159, N'Is Mike Martins Software Cool', 327)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (160, N'Is Mike Martins Software Cool', 328)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (161, N'Is Mike Martins Software Cool', 329)
GO
INSERT [dbo].[Question] ([QuestionID], [QueDetail], [VideoID]) VALUES (162, N'How Cool Is This? ', 330)
GO
SET IDENTITY_INSERT [dbo].[Question] OFF
GO
SET IDENTITY_INSERT [dbo].[UserDetail] ON 

GO
INSERT [dbo].[UserDetail] ([UserId], [UserName], [UserPassword], [UserEmail], [RoleId]) VALUES (1, N'admin', N'admin', N'admin@adm.com', 1)
GO
INSERT [dbo].[UserDetail] ([UserId], [UserName], [UserPassword], [UserEmail], [RoleId]) VALUES (2, N'test', N'test', N'test@test.com', 2)
GO
INSERT [dbo].[UserDetail] ([UserId], [UserName], [UserPassword], [UserEmail], [RoleId]) VALUES (4, N'admin', N'admin', N'paultruscott@hotmail.com', 2)
GO
INSERT [dbo].[UserDetail] ([UserId], [UserName], [UserPassword], [UserEmail], [RoleId]) VALUES (5, N'Arpan', N'arpan1823', N'arpandev1823@gmail.com', 2)
GO
INSERT [dbo].[UserDetail] ([UserId], [UserName], [UserPassword], [UserEmail], [RoleId]) VALUES (6, N'test', N'Test', N'arpanpatel1823@gmail.com', 2)
GO
INSERT [dbo].[UserDetail] ([UserId], [UserName], [UserPassword], [UserEmail], [RoleId]) VALUES (7, N'Test', N'Test@123', N'test123@test.com', 2)
GO
SET IDENTITY_INSERT [dbo].[UserDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[UserRole] ON 

GO
INSERT [dbo].[UserRole] ([RoleId], [RoleName]) VALUES (1, N'Admin')
GO
INSERT [dbo].[UserRole] ([RoleId], [RoleName]) VALUES (2, N'User')
GO
SET IDENTITY_INSERT [dbo].[UserRole] OFF
GO
SET IDENTITY_INSERT [dbo].[Video] ON 

GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (1, N'https://vimeo.com/52273613', 1, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (2, N'https://vimeo.com/44056926', 1, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (3, N'https://vimeo.com/52273613', 2, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (4, N'https://vimeo.com/228071975', 2, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (5, N'https://www.youtube.com/watch?v=eUhzDf9eRbM', 3, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (6, N'https://vimeo.com/232323497', 3, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (7, N'https://www.youtube.com/watch?v=eUhzDf9eRbM', 3, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (8, N'https://vimeo.com/232323497', 3, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (9, N'https://vimeo.com/52273613', 4, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (10, N'https://vimeo.com/44056926', 4, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (11, N'https://vimeo.com/44056926', 5, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (12, N'https://vimeo.com/44056926', 5, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (13, N'https://www.youtube.com/watch?v=B6BJM20ra6A', 6, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (14, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 6, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (15, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 6, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (16, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 6, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (17, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 7, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (18, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 7, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (19, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 7, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (20, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 7, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (21, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 8, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (22, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 8, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (23, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 8, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (24, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 8, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (25, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 9, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (26, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 9, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (27, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 9, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (28, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 9, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (29, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 10, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (30, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 10, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (31, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 10, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (32, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 10, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (33, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 11, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (34, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 11, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (35, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 11, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (36, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 11, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (37, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 12, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (38, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 12, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (39, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 12, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (40, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 12, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (41, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 13, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (42, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 13, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (43, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 13, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (44, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 13, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (45, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 14, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (46, N'https://www.youtube.com/watch?v=0G2RlRLfVVk', 14, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (47, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 14, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (48, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 14, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (49, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 15, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (50, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 15, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (51, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 15, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (52, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 15, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (53, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 16, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (54, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 16, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (55, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 16, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (56, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 16, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (57, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 17, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (58, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 17, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (59, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 17, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (60, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 17, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (61, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 18, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (62, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 18, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (63, N'https://www.youtube.com/watch?v=SIDbsp1nQRs', 18, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (64, N'https://www.youtube.com/watch?v=gOg1M8PPGuU', 18, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (65, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 19, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (66, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 19, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (67, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 19, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (68, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 19, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (69, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 20, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (70, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 20, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (71, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 20, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (72, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 20, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (73, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 21, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (74, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 21, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (75, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 21, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (76, N'https://www.youtube.com/watch?v=ceNYKhq3gtc', 21, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (77, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 22, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (78, N'https://www.youtube.com/watch?v=RzRyij4kWEs', 22, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (79, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 22, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (80, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 22, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (81, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 23, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (82, N'https://www.youtube.com/watch?v=RzRyij4kWEs', 23, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (83, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 23, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (84, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 23, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (85, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 24, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (86, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 24, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (87, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 24, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (88, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 24, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (154, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 33, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (155, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 33, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (156, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 33, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (157, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 33, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (158, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 34, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (159, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 34, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (160, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 34, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (161, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 34, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (162, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 35, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (163, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 35, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (164, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 35, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (165, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 35, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (166, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 36, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (167, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 36, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (168, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 36, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (169, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 36, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (170, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 37, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (171, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 37, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (172, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 37, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (173, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 37, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (174, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 38, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (175, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 38, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (176, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 38, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (177, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 38, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (178, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 39, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (179, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 40, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (180, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 40, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (181, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 41, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (182, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 41, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (183, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 42, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (184, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 42, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (185, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 43, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (186, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 43, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (187, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 43, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (188, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 43, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (189, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 44, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (190, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 44, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (191, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 44, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (192, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 44, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (193, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 45, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (194, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 45, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (195, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 45, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (196, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 45, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (197, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 46, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (198, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 46, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (199, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 46, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (200, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 46, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (201, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 46, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (202, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 47, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (203, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 47, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (204, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 47, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (205, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 47, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (206, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 47, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (207, N'https://www.youtube.com/watch?v=WqUrSgjq_6E', 47, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (216, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 50, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (217, N'https://www.youtube.com/watch?v=KrdVJdRApJI', 50, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (218, N'https://www.youtube.com/watch?v=RzRyij4kWEs', 50, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (219, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 50, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (220, N'https://vimeo.com/52273613', 51, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (221, N'https://vimeo.com/44056926', 51, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (222, N'https://vimeo.com/213083488', 51, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (223, N'https://vimeo.com/213083488', 51, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (224, N'https://vimeo.com/228071975', 51, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (225, N'https://vimeo.com/52273613', 52, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (226, N'https://vimeo.com/44056926', 52, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (227, N'https://vimeo.com/213083488', 52, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (228, N'https://vimeo.com/213083488', 52, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (229, N'https://vimeo.com/228071975', 52, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (230, N'https://vimeo.com/52273613', 53, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (231, N'https://vimeo.com/44056926', 53, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (232, N'https://vimeo.com/213083488', 53, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (233, N'https://vimeo.com/213083488', 53, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (234, N'https://vimeo.com/228071975', 53, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (235, N'https://vimeo.com/52273613', 54, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (236, N'https://vimeo.com/44056926', 54, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (237, N'https://vimeo.com/213083488', 54, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (238, N'https://vimeo.com/213083488', 54, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (239, N'https://vimeo.com/228071975', 54, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (240, N'https://vimeo.com/213083488', 55, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (241, N'https://vimeo.com/213083488', 55, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (244, N'https://vimeo.com/213083488', 57, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (245, N'https://vimeo.com/213083488', 57, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (246, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 58, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (247, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 58, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (248, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 58, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (249, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 58, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (250, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 59, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (251, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 59, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (252, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 59, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (253, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 59, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (254, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 60, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (255, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 60, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (256, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 60, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (257, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 60, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (258, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 61, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (259, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 61, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (260, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 61, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (261, N'https://www.youtube.com/watch?v=mZdDDR5yjRc', 61, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (262, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 62, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (263, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 62, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (264, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 62, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (265, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 62, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (266, N'https://www.youtube.com/watch?v=lSWYTKqtctg', 63, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (267, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 63, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (268, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 63, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (269, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 63, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (270, N'https://www.youtube.com/watch?v=AAbUvs28Ou8', 64, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (271, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 64, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (272, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 64, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (273, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 64, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (274, N'https://www.youtube.com/watch?v=AAbUvs28Ou8', 65, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (275, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 65, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (276, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 65, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (277, N'https://www.youtube.com/watch?v=3CTTFZe2tYM', 65, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (278, N'https://www.youtube.com/watch?v=RzRyij4kWEs', 66, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (279, N'https://www.youtube.com/watch?v=RzRyij4kWEs', 66, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (280, N'https://www.youtube.com/watch?v=RzRyij4kWEs', 66, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (281, N'https://www.youtube.com/watch?v=RzRyij4kWEs', 66, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (282, N'https://vimeo.com/52273613', 67, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (283, N'https://vimeo.com/44056926', 67, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (284, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 68, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (285, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 68, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (286, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 68, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (287, N'https://www.youtube.com/watch?v=WBu91oiRJkI', 68, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (288, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 69, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (289, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 69, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (290, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 69, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (291, N'https://www.youtube.com/watch?v=1ZWNEVM_Mp4', 69, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (292, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 70, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (293, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 70, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (294, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 70, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (295, N'https://www.youtube.com/watch?v=v2y9quPaAvk', 70, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (296, N'https://vimeo.com/25456755', 71, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (297, N'https://vimeo.com/103633309', 71, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (298, N'https://vimeo.com/52273613', 72, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (299, N'https://vimeo.com/52273613', 72, 0)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (300, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 73, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (301, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 74, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (302, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 75, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (303, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 76, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (304, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 77, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (305, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 78, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (306, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 79, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (307, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 80, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (308, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 81, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (309, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 82, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (310, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 83, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (311, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 84, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (312, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 85, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (313, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 86, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (314, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 87, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (315, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 88, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (316, N'https://www.youtube.com/watch?v=QzFQQUI1i_c', 89, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (317, N'https://www.youtube.com/watch?v=BzYwtqeFr3c', 90, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (318, N'https://www.youtube.com/watch?v=BzYwtqeFr3c', 91, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (319, N'https://www.youtube.com/watch?v=BzYwtqeFr3c', 92, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (320, N'https://www.youtube.com/watch?v=BzYwtqeFr3c', 93, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (321, N'https://www.youtube.com/watch?v=BzYwtqeFr3c', 94, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (322, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 95, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (323, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 96, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (324, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 97, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (325, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 98, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (326, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 99, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (327, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 100, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (328, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 101, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (329, N'https://www.youtube.com/watch?v=J9PKWte4eRI', 102, 1)
GO
INSERT [dbo].[Video] ([VideoID], [URL], [VideoCampaignID], [IsStart]) VALUES (330, N'https://www.youtube.com/watch?v=SBMrLnghkEI', 102, 0)
GO
SET IDENTITY_INSERT [dbo].[Video] OFF
GO
SET IDENTITY_INSERT [dbo].[VideoCampaign] ON 

GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (1, N'Test', 1, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (2, N'Test 123', 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (3, N'DeveloperTest', 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (4, N'Test', 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (5, N'Test_123', 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (6, N'test 1', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (7, N'test 1', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (8, N'test 1', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (9, N'test 2', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (10, N'test 2', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (11, N'test 2', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (12, N'test 2', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (13, N'test 2', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (14, N'test 3', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (15, N'test', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (16, N'test3', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (17, N'test4', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (18, N'test 4', 1, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (19, N'test', 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (20, N'test', 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (21, N'test', 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (22, N'test', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (23, N'test', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (24, N'pest test', 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (33, N'test bugs', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (34, N'test bugs', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (35, N'test bugs', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (36, N'test bugs', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (37, N'test bugs', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (38, N'test bugs', 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (39, N'test', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (40, N'test', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (41, N'bugs', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (42, N'bugs', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (43, N'bugs test', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (44, N'bugs test 3', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (45, N'test 2', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (46, N'test 2', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (47, N'test 2', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (50, N'test1', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (51, N'Test 1234', 1, 5, CAST(N'2017-09-24 11:40:36.663' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (52, N'Test 1234', 1, 5, CAST(N'2017-09-24 12:00:42.243' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (53, N'Test 1234', 1, 5, CAST(N'2017-09-24 12:00:59.677' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (54, N'Test 1234qqq', 1, 5, CAST(N'2017-09-24 12:01:11.030' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (55, N'Test12345', 1, 5, CAST(N'2017-09-24 12:07:02.843' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (57, N'Test', 1, 5, CAST(N'2017-09-24 12:37:30.113' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (58, N'test', 1, 3, CAST(N'2017-09-25 02:17:16.380' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (59, N'test', 1, 3, CAST(N'2017-09-25 02:17:24.207' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (60, N'test 2', 1, 32, CAST(N'2017-09-25 02:19:57.700' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (61, N'test 2', 1, 4, CAST(N'2017-09-25 02:20:36.130' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (62, N'tester', 1, 4, CAST(N'2017-09-25 02:28:34.517' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (63, N'tester', 1, 4, CAST(N'2017-09-25 02:31:53.537' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (64, N'tester', 1, 4, CAST(N'2017-09-25 02:35:04.037' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (65, N'tester', 1, 4, CAST(N'2017-09-25 02:44:27.077' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (66, N'test', 1, 3, CAST(N'2017-10-02 00:40:44.370' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (67, N'Test', 1, 5, CAST(N'2017-10-02 03:18:21.490' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (68, N'tester', 1, 3, CAST(N'2017-10-02 04:22:32.257' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (69, N'tester 1', 1, 3, CAST(N'2017-10-09 10:40:09.490' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (70, N'tester 1', 1, 3, CAST(N'2017-10-09 10:41:26.260' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (71, N'Test Color', 2, 4, CAST(N'2017-10-28 07:02:44.853' AS DateTime), N'#eb0903', N'#bbc271', N'#f6055b', N'#c7d3cc', N'#7b7dc2', N'#022b0a')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (72, N'Test_Dev', 1, 11, CAST(N'2017-10-28 08:39:00.340' AS DateTime), N'#fafa87', N'#292b2c', N'#68e719', N'#1d89ff', N'#e7dada', N'#1d6bff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (73, N'Leee John', 1, 10, CAST(N'2017-11-28 11:26:29.850' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (74, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:31:31.790' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (75, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:33:47.637' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (76, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:37:07.403' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (77, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:39:43.870' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (78, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:40:27.707' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (79, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:43:48.153' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (80, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:44:49.280' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (81, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:46:08.153' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (82, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:47:22.837' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (83, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:51:14.817' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (84, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:53:14.800' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (85, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:55:25.750' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (86, N'Leee John 2', 1, 10, CAST(N'2017-11-28 11:57:21.360' AS DateTime), N'#53f649', N'#292b2c', N'#ee0d14', N'#ffffff', N'#ffffff', N'#2aff1d')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (87, N'Leee John 2', 1, 10, CAST(N'2017-11-28 12:01:01.140' AS DateTime), N'#495cf6', N'#292b2c', N'#4cee0d', N'#ffffff', N'#ffffff', N'#3beb12')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (88, N'Leee John 2', 1, 10, CAST(N'2017-11-28 12:02:40.393' AS DateTime), N'#8c089a', N'#292b2c', N'#ee830d', N'#ffffff', N'#ffffff', N'#eb7a12')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (89, N'Leee John 2', 1, 30, CAST(N'2017-11-28 12:03:55.100' AS DateTime), N'#8c089a', N'#292b2c', N'#ee830d', N'#ffffff', N'#ffffff', N'#eb7a12')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (90, N'Leee John Gig', 1, 15, CAST(N'2017-12-02 09:27:41.640' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (91, N'Leee John Gig', 1, 15, CAST(N'2017-12-02 09:29:57.667' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (92, N'Leee John Gig', 1, 15, CAST(N'2017-12-02 09:32:03.217' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (93, N'Leee John Gig', 1, 15, CAST(N'2017-12-02 09:34:37.493' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (94, N'Leee John Gig', 1, 15, CAST(N'2017-12-02 09:39:10.050' AS DateTime), N'#e97232', N'#292b2c', N'#ffffff', N'#b30fd8', N'#ffffff', N'#b81dff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (95, N'Michael Martin', 1, 56, CAST(N'2018-07-03 12:45:16.677' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (96, N'Michael Martin', 1, 56, CAST(N'2018-07-03 12:55:58.520' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (97, N'Michael Martin', 1, 56, CAST(N'2018-07-03 13:01:38.993' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (98, N'Michael Martin', 1, 56, CAST(N'2018-07-03 13:05:13.663' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (99, N'Michael Martin', 1, 56, CAST(N'2018-07-03 13:09:53.360' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (100, N'Michael Martin', 1, 56, CAST(N'2018-07-03 13:14:15.917' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (101, N'Michael Martin', 1, 56, CAST(N'2018-07-03 13:17:42.120' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
INSERT [dbo].[VideoCampaign] ([VideoCampaignID], [Name], [UserId], [QuestionDuration], [CreatedDate], [QueBG], [QueFont], [AnsBG], [AnsFont], [AnsHoverFont], [AnsHoverBG]) VALUES (102, N'Michael Martin', 1, 56, CAST(N'2018-07-03 13:26:19.157' AS DateTime), N'lightskyblue', N'#292b2c', N'#ffffff', N'#1d89ff', N'#ffffff', N'#1d89ff')
GO
SET IDENTITY_INSERT [dbo].[VideoCampaign] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UK_UserDetail]    Script Date: 8/19/2018 4:11:23 PM ******/
ALTER TABLE [dbo].[UserDetail] ADD  CONSTRAINT [UK_UserDetail] UNIQUE NONCLUSTERED 
(
	[UserEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Question] FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Question] ([QuestionID])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Question]
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Video] FOREIGN KEY([VideoID])
REFERENCES [dbo].[Video] ([VideoID])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Video]
GO
ALTER TABLE [dbo].[Button]  WITH CHECK ADD  CONSTRAINT [FK_Button_Button] FOREIGN KEY([ButtonSetID])
REFERENCES [dbo].[ButtonSet] ([ButtonSetID])
GO
ALTER TABLE [dbo].[Button] CHECK CONSTRAINT [FK_Button_Button]
GO
ALTER TABLE [dbo].[ButtonSet]  WITH CHECK ADD  CONSTRAINT [FK_ButtonSet_UserDetail] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetail] ([UserId])
GO
ALTER TABLE [dbo].[ButtonSet] CHECK CONSTRAINT [FK_ButtonSet_UserDetail]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Video] FOREIGN KEY([VideoID])
REFERENCES [dbo].[Video] ([VideoID])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Video]
GO
ALTER TABLE [dbo].[UserDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserDetail_UserRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[UserRole] ([RoleId])
GO
ALTER TABLE [dbo].[UserDetail] CHECK CONSTRAINT [FK_UserDetail_UserRole]
GO
ALTER TABLE [dbo].[Video]  WITH CHECK ADD  CONSTRAINT [FK_Video_VideoCampaign] FOREIGN KEY([VideoCampaignID])
REFERENCES [dbo].[VideoCampaign] ([VideoCampaignID])
GO
ALTER TABLE [dbo].[Video] CHECK CONSTRAINT [FK_Video_VideoCampaign]
GO
ALTER TABLE [dbo].[VideoCampaign]  WITH CHECK ADD  CONSTRAINT [FK_VideoCampaign_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserDetail] ([UserId])
GO
ALTER TABLE [dbo].[VideoCampaign] CHECK CONSTRAINT [FK_VideoCampaign_User]
GO
/****** Object:  StoredProcedure [dbo].[sp_buttonsetdetail_save]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Parth Patel
-- Create date: 15 Aug 2018
-- Description:	Insert ButtonSet Detail
-- =============================================
CREATE PROCEDURE [dbo].[sp_buttonsetdetail_save] 
	@xml_ButtonSetModel XML
AS
BEGIN

	DECLARE @UserID INT, @ButtonSetName VARCHAR(MAX), @IsDisplayHorizontal BIT

	DECLARE @tbl_Buttons TABLE
	(
		ButtonText VARCHAR(MAX),
		ButtonBackgroundColor VARCHAR(50),
		ButtonFontColor VARCHAR(50),
		ButtonHoverBackgroundColor VARCHAR(50),
		ButtonHoverFontColor VARCHAR(50),
		ButtonLink VARCHAR(MAX)
	)
	
	DECLARE @docHandle int
	EXEC sp_xml_preparedocument @docHandle OUTPUT, @xml_ButtonSetModel  
	
	SELECT @UserID = UserID
		, @ButtonSetName = ButtonSetName
		, @IsDisplayHorizontal = IsDisplayHorizontal
	FROM OPENXML (@docHandle, '/ButtonSetModel', 2)  
	WITH (UserID  INT,
		ButtonSetName VARCHAR(MAX),
		IsDisplayHorizontal BIT)
	
	INSERT INTO @tbl_Buttons
	SELECT ButtonText,
		   ButtonBackgroundColor,
		   ButtonFontColor,
		   ButtonHoverBackgroundColor,
		   ButtonHoverFontColor,
		   ButtonLink
	FROM OPENXML (@docHandle, '/ButtonSetModel/lstButtons/ButtonModel', 2)
	WITH (ButtonText VARCHAR(MAX),
		ButtonBackgroundColor VARCHAR(50),
		ButtonFontColor VARCHAR(50),
		ButtonHoverBackgroundColor VARCHAR(50),
		ButtonHoverFontColor VARCHAR(50),
		ButtonLink VARCHAR(MAX))  

	EXEC sp_xml_removedocument @docHandle  

	INSERT INTO ButtonSet(ButtonSetName, IsDisplayHorizontal,UserID) VALUES(@ButtonSetName, @IsDisplayHorizontal, @UserID)

	DECLARE @ButtonSetID INT
	SELECT TOP 1 @ButtonSetID = ButtonSetID FROM ButtonSet ORDER BY ButtonSetID DESC

	INSERT INTO Button(ButtonText, ButtonBackgroundColor, ButtonFontColor,
		   ButtonHoverBackgroundColor, ButtonHoverFontColor, ButtonLink, ButtonSetID)
	SELECT ButtonText, ButtonBackgroundColor, ButtonFontColor,
		   ButtonHoverBackgroundColor, ButtonHoverFontColor, ButtonLink, @ButtonSetID
		FROM @tbl_Buttons

END
GO
/****** Object:  StoredProcedure [dbo].[sp_buttonsetdetail_select_by_buttonsetid]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Parth Patel
-- Create date: 19 Aug 2018
-- Description: Select Button Set Detail to Display
-- =============================================
CREATE PROCEDURE [dbo].[sp_buttonsetdetail_select_by_buttonsetid]
	@ButtonSetID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM ButtonSet WHERE ButtonSetID = @ButtonSetID

	SELECT * FROM Button WHERE ButtonSetID = @ButtonSetID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_buttonsetlist_select_by_user]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Parth Patel
-- Create date: 19 Aug 2018
-- Description:	Get All Button Set for User
-- =============================================
CREATE PROCEDURE [dbo].[sp_buttonsetlist_select_by_user]
	@UserID INT
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT * FROM ButtonSet WHERE UserID = @UserID ORDER BY ButtonSetID DESC
	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_userdetail_insert]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Parth Patel
-- Create date: 3 Oct 2017
-- Description:	Insert User Detail
-- =============================================
CREATE PROCEDURE [dbo].[sp_userdetail_insert]
	-- Add the parameters for the stored procedure here
	@UserName varchar(50)
	,@UserPassword varchar(50)
	,@UserEmail varchar(50)
	,@UserRole varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @RoleId INT

	SELECT @RoleId=RoleId FROM UserRole WHERE RoleName=@UserRole

	INSERT INTO UserDetail(UserName,UserPassword,UserEmail,RoleId)
	VALUES(@UserName,@UserPassword,@UserEmail,@RoleId)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_userdetail_select_by_useremail]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_userdetail_select_by_useremail]
	-- Add the parameters for the stored procedure here
	@UserEmail varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ud.*,ur.RoleName from UserDetail ud inner join
	 UserRole ur on ud.RoleId = ur.RoleId
	 where UserEmail = @UserEmail 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_userdetail_select_by_username_password]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Parth
-- Create date: 22 July 2017
-- Description:	Get UserDetail For Login
-- =============================================
CREATE PROCEDURE [dbo].[sp_userdetail_select_by_username_password]
	-- Add the parameters for the stored procedure here
	@UserEmail VARCHAR(50),
	@UserPassword VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM UserDetail(NOLOCK) WHERE UserEmail=@UserEmail AND UserPassword=@UserPassword
END

GO
/****** Object:  StoredProcedure [dbo].[sp_VideoCampaignList_select_by_user]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Parth Patel
-- Create date: 12 September 2017
-- Description:	sp_VideoCampaignList_select_by_user
-- =============================================
CREATE PROCEDURE [dbo].[sp_VideoCampaignList_select_by_user]
	-- Add the parameters for the stored procedure here
	@UserId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		vc.VideoCampaignID,
		Name,
		QuestionDuration,
		CONVERT(VARCHAR(10), CreatedDate, 101) AS CreatedDate,
		v.URL
	FROM VideoCampaign vc WITH(NOLOCK)
	inner join Video v on v.VideoCampaignID = vc.VideoCampaignID
	WHERE UserId = @UserId and v.IsStart = 1
END

GO
/****** Object:  StoredProcedure [dbo].[sp_videodetail_save_videohierarchy_capaigndetail]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_videodetail_save_videohierarchy_capaigndetail]
	-- Add the parameters for the stored procedure here
	@tableSave [SetCampaignDetail] readonly,
	@videoCampaignId int = -1,
	@VideoCampaignName nvarchar(50) = 'Test 2',
	@UserId int = 1,
	@QuestionDuration int = 5,
	@QueBG varchar(20),
	@QueFont varchar(20),
	@AnsBG varchar(20),
	@AnsFont varchar(20),
	@AnsHoverBG varchar(20),
	@AnsHoverFont varchar(20)
AS
BEGIN
	
	if(@videoCampaignId<=0)
	begin
		INSERT into VideoCampaign values(@VideoCampaignName,@UserId,@QuestionDuration,GETDATE(),@QueBG,@QueFont,@AnsBG,@AnsFont,@AnsHoverFont,@AnsHoverBG)
		set @videoCampaignId = SCOPE_IDENTITY()
	end
	else
	begin
		update VideoCampaign set Name = @VideoCampaignName where VideoCampaignID = @videoCampaignId
	end
	
	declare @tableTemp setCampaignDetail
	
	--insert into @tableTemp 
	--values(0,NULL,'Main Video Que ?',NULL,'https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null),
	--(2,0,'L1 V1 que 1?','Main Video Ans 1','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null),
	--(4,0,'L1 V2 que 2?','Main Video Ans 2','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null),
	--(5,2,NULL,'L1 V1 Ans 1','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null),
	--(7,2,NULL,'L1 V1 Ans 2','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null),
	--(14,4,NULL,'L1 V2 Ans 1','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null)
	
	insert into @tableTemp select * from @tableSave
	
	;WITH OrderedOrders AS  
(  
    SELECT *,  
    ROW_NUMBER() OVER (ORDER BY [selfID]) AS RowNumber  
    FROM @tableTemp   
)   

	select * into #temp from OrderedOrders
	
	declare @countTotal int = (select COUNT(*) from #temp)
	
	declare @startCount int = 1
	
	while(@countTotal>=@startCount)
	
	begin
		declare @selfId int, @parentId int, 
				@QuestionText nvarchar(max),@AnswerText nvarchar(max),@VideoUrl nvarchar(max),
				@VideoId int,@QuestionId int,@AnswerId int,
				@RedirectVideoText varchar(max), @RedirectVideoType varchar(50)
				
		select @selfId = t.selfId,@parentId = t.parentId, 
				@QuestionText = t.QuestionText,@AnswerText = t.AnswerText,@VideoUrl = t.VideoUrl,
				@VideoId = t.VideoId,@QuestionId = t.QuestionId,@AnswerId = t.AnswerId,
				@RedirectVideoText = t.RedirectVideoText, @RedirectVideoType = t.RedirectVideoType
				from #temp t where RowNumber = @startCount
		
		declare @IsStart bit = 0
		
		if(@selfId = 0)
		begin
			set @Isstart = 1
		end
		if(ISNULL(@VideoId, 0)>0)
		begin
			if(ISNULL(@VideoUrl,'') != '')
			begin
				update Video set URL = @VideoUrl,
							VideoCampaignID = @videoCampaignId,
							IsStart = @IsStart
							 where VideoID = @VideoId
			end
		end
		else
		begin
			if(ISNULL(@VideoUrl,'') != '')
			begin
				insert into Video values(@VideoUrl,@videoCampaignId,@IsStart)
			
				set @VideoId = SCOPE_IDENTITY()
				update #temp set videoId = @VideoId where RowNumber = @startCount
			end
		end
		if(ISNULL(@QuestionText,'')!='')
			begin
			if(ISNULL(@QuestionId,0)>0)
			begin
				update Question set QueDetail = @QuestionText,
								VideoID = @VideoId 
								where QuestionID = @QuestionId
			end
			else
			begin
				
					insert into Question values(@QuestionText,@VideoId)
					set @QuestionId = SCOPE_IDENTITY()
					update #temp set QuestionId = @QuestionId where selfID = @selfId and ISNULL(RedirectVideoType,'') = ISNULL(@RedirectVideoType,'')

				end
		end
		if(ISNULL(@AnswerText,'')!='')
			begin
		declare @tempQuestionId int = (select QuestionId from #temp where selfId = @parentId)
		if(ISNULL(@AnswerId,0)>0)
		begin
			update Answer set AnsDetail = @AnswerText,
								QuestionID = @tempQuestionId,
								VideoID = @VideoId,
								RedirectVideoText = @RedirectVideoText,
								RedirectVideoType = @RedirectVideoType
								where AnswerID = @AnswerId			
		end
		else
		begin
				insert into Answer values(@AnswerText,@tempQuestionId,@VideoId,@RedirectVideoText,@RedirectVideoType)
				set @AnswerId = SCOPE_IDENTITY()
				update #temp set AnswerId = @AnswerId where selfID = @selfId and ISNULL(RedirectVideoType,'') = ISNULL(@RedirectVideoType,'')
			end
		end
		
		
		set @startCount = @startCount + 1
	end
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select * from #temp
	
	delete @tableTemp
	drop table #temp

	SELECT TOP 1 * FROM VideoCampaign
	WHERE Name = @VideoCampaignName AND UserId = @UserId
	ORDER BY VideoCampaignID DESC
END

-- exec sp__videodetail_save_videohierarchy_capaigndetail

GO
/****** Object:  StoredProcedure [dbo].[sp_videohierarchy_select_by_video_campaign_id]    Script Date: 8/19/2018 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		PARTH
-- Create date: 06 AUGUST 2017
-- Description:	GET VIDEO HIERARCHY BY VideoCampaignID
-- =============================================
CREATE PROCEDURE [dbo].[sp_videohierarchy_select_by_video_campaign_id]
	-- Add the parameters for the stored procedure here
	@VideoCampaignID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Campagian Detail
	SELECT
		VC.VideoCampaignID,VC.Name,VC.UserId
		,ISNULL(VC.QuestionDuration,-1) as QuestionDuration
		,VC.CreatedDate,VC.QueBG
		,VC.QueFont,VC.AnsBG
		,VC.AnsFont,VC.AnsHoverBG,VC.AnsHoverFont
	FROM VideoCampaign(NOLOCK) VC
	WHERE VC.VideoCampaignID = @VideoCampaignID

	-- Video Hierarchy
	SELECT 
		V.VideoID,V.IsStart
		,V.URL AS VideoURL
		,ISNULL(Q.QuestionID,-1) AS QuestionID
		,Q.QueDetail
		,ISNULL(A.AnswerID,-1) AS AnswerID
		,A.AnsDetail
		,ISNULL(A.VideoID,-1) AS NextVideoID
		,A.RedirectVideoText
		,A.RedirectVideoType
	FROM VideoCampaign(NOLOCK) VC
	INNER JOIN Video(NOLOCK) V ON V.VideoCampaignID = VC.VideoCampaignID
	LEFT JOIN Question(NOLOCK) Q ON Q.VideoID = V.VideoID
	LEFT JOIN Answer(NOLOCK) A ON A.QuestionID = Q.QuestionID
	WHERE VC.VideoCampaignID = @VideoCampaignID
	
END

GO
USE [master]
GO
ALTER DATABASE [VideoSoftware] SET  READ_WRITE 
GO
