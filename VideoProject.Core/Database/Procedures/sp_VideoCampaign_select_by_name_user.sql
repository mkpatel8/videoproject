-- =============================================
-- Author:		Parth Patel
-- Create date: 8 Aug 2017
-- Description:	sp_VideoCampaign_select_by_name_user
-- =============================================
ALTER PROCEDURE [dbo].[sp_VideoCampaign_select_by_name_user]
	-- Add the parameters for the stored procedure here
	@UserId INT,
	@VideoCampaignName nVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM VideoCampaign WITH(NOLOCK)
	WHERE Name = @VideoCampaignName AND UserId = @UserId
END
