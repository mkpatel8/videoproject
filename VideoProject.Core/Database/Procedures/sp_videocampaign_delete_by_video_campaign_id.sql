IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[sp_videocampaign_delete_by_video_campaign_id]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
DROP PROCEDURE [dbo].[sp_videocampaign_delete_by_video_campaign_id]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		MEHUL PATEL
-- Create date: 22 Mar 2020
-- Description:	Delete VIDEO From VideoCampaign List with ID
-- =============================================
CREATE PROCEDURE [dbo].[sp_videocampaign_delete_by_video_campaign_id]
	-- Add the parameters for the stored procedure here
	@VideoCampaignID INT
AS
BEGIN
	SET NOCOUNT ON;

BEGIN TRANSACTION 

DELETE FROM Answer WHERE QuestionID IN (SELECT QuestionId FROM Question WHERE VideoID in (select VideoID from Video where VideoCampaignID = @VideoCampaignID))
DELETE FROM Question WHERE VideoID IN (SELECT VideoID FROM Video WHERE VideoCampaignID = @VideoCampaignID)
DELETE FROM Video WHERE VideoCampaignID = @VideoCampaignID
DELETE FROM VideoCampaign WHERE VideoCampaignID = @VideoCampaignID

COMMIT  TRANSACTION
	
END
