USE [VideoSoftware]
GO
/****** Object:  StoredProcedure [dbo].[sp_videohierarchy_select_by_video_campaign_id]    Script Date: 10/21/2017 16:44:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		PARTH
-- Create date: 06 AUGUST 2017
-- Description:	GET VIDEO HIERARCHY BY VideoCampaignID
-- =============================================
ALTER PROCEDURE [dbo].[sp_videohierarchy_select_by_video_campaign_id]
	-- Add the parameters for the stored procedure here
	@VideoCampaignID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Campagian Detail
	SELECT
		VC.VideoCampaignID,VC.Name,VC.UserId
		,ISNULL(VC.QuestionDuration,-1) as QuestionDuration
		,VC.CreatedDate,VC.QueBG
		,VC.QueFont,VC.AnsBG
		,VC.AnsFont,VC.AnsHoverBG,VC.AnsHoverFont
	FROM VideoCampaign(NOLOCK) VC
	WHERE VC.VideoCampaignID = @VideoCampaignID

	-- Video Hierarchy
	SELECT 
		V.VideoID,V.IsStart
		,V.URL AS VideoURL
		,ISNULL(Q.QuestionID,-1) AS QuestionID
		,Q.QueDetail
		,ISNULL(A.AnswerID,-1) AS AnswerID
		,A.AnsDetail
		,ISNULL(A.VideoID,-1) AS NextVideoID
		,A.RedirectVideoText
		,A.RedirectVideoType
	FROM VideoCampaign(NOLOCK) VC
	INNER JOIN Video(NOLOCK) V ON V.VideoCampaignID = VC.VideoCampaignID
	LEFT JOIN Question(NOLOCK) Q ON Q.VideoID = V.VideoID
	LEFT JOIN Answer(NOLOCK) A ON A.QuestionID = Q.QuestionID
	WHERE VC.VideoCampaignID = @VideoCampaignID
	
END