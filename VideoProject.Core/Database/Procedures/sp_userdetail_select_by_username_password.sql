USE [VideoSoftware]
GO
/****** Object:  StoredProcedure [dbo].[sp_userdetail_select_by_username_password]    Script Date: 10/08/2017 16:06:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Parth
-- Create date: 22 July 2017
-- Description:	Get UserDetail For Login
-- =============================================
ALTER PROCEDURE [dbo].[sp_userdetail_select_by_username_password]
	-- Add the parameters for the stored procedure here
	@UserEmail VARCHAR(50),
	@UserPassword VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM UserDetail(NOLOCK) WHERE UserEmail=@UserEmail AND UserPassword=@UserPassword
END

