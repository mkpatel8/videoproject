USE [VideoSoftware]
GO
/****** Object:  StoredProcedure [dbo].[sp_videodetail_save_videohierarchy_capaigndetail]    Script Date: 10/30/2019 19:10:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_videodetail_save_videohierarchy_capaigndetail]
	-- Add the parameters for the stored procedure here
	@tableSave [SetCampaignDetail] readonly,
	@videoCampaignId int = -1,
	@VideoCampaignName nvarchar(50) = 'Test 2',
	@UserId int = 1,
	@QuestionDuration int = 5,
	@QueBG varchar(20) = null,
	@QueFont varchar(20) = null,
	@AnsBG varchar(20)= null,
	@AnsFont varchar(20)= null,
	@AnsHoverBG varchar(20)= null,
	@AnsHoverFont varchar(20)= null
AS
BEGIN
	
	if(@videoCampaignId<=0)
	begin
		INSERT into VideoCampaign values(@VideoCampaignName,@UserId,@QuestionDuration,GETDATE(),@QueBG,@QueFont,@AnsBG,@AnsFont,@AnsHoverFont,@AnsHoverBG)
		set @videoCampaignId = SCOPE_IDENTITY()
	end
	else
	begin
		update VideoCampaign set Name = @VideoCampaignName where VideoCampaignID = @videoCampaignId
	end
	
	declare @tableTemp setCampaignDetail
	
	--insert into @tableTemp 
	--values(1,NULL,'Main Video Que ?',NULL,'https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null,null,null),
	--(2,1,'L1 V1 que 1?','Main Video Ans 1','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null,null,null),
	--(4,1,'L1 V2 que 2?','Main Video Ans 2','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null,null,null),
	--(5,2,NULL,'L1 V1 Ans 1','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null,null,null),
	--(7,2,NULL,'L1 V1 Ans 2','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null,null,null),
	--(14,4,NULL,'L1 V2 Ans 1','https://www.youtube.com/watch?v=jJBJXjyW65w',Null,null,null,null,null)
	
	insert into @tableTemp select * from @tableSave
	
	;WITH OrderedOrders AS  
(  
    SELECT *,  
    ROW_NUMBER() OVER (ORDER BY [selfID]) AS RowNumber  
    FROM @tableTemp   
)   

	select * into #temp from OrderedOrders
	
	declare @countTotal int = (select COUNT(*) from #temp)
	
	declare @startCount int = 1
	
	while(@countTotal>=@startCount)
	
	begin
		declare @selfId int, @parentId int, 
				@QuestionText nvarchar(max),@AnswerText nvarchar(max),@VideoUrl nvarchar(max),
				@VideoId int,@QuestionId int,@AnswerId int,
				@RedirectVideoText varchar(max), @RedirectVideoType varchar(50)
				
		select @selfId = t.selfId,@parentId = t.parentId, 
				@QuestionText = t.QuestionText,@AnswerText = t.AnswerText,@VideoUrl = t.VideoUrl,
				@VideoId = t.VideoId,@QuestionId = t.QuestionId,@AnswerId = t.AnswerId,
				@RedirectVideoText = t.RedirectVideoText, @RedirectVideoType = t.RedirectVideoType
				from #temp t where RowNumber = @startCount
		
		declare @IsStart bit = 0
		
		if(@selfId = 0 or @selfId is null)
		begin
			set @Isstart = 1
		end
		if(ISNULL(@VideoId, 0)>0)
		begin
			if(ISNULL(@VideoUrl,'') != '')
			begin
				update Video set URL = @VideoUrl,
							VideoCampaignID = @videoCampaignId,
							IsStart = @IsStart
							 where VideoID = @VideoId
			end
		end
		else
		begin
			if(ISNULL(@VideoUrl,'') != '')
			begin
				insert into Video values(@VideoUrl,@videoCampaignId,@IsStart)
			
				set @VideoId = SCOPE_IDENTITY()
				update #temp set videoId = @VideoId where RowNumber = @startCount
			end
		end
		if(ISNULL(@QuestionText,'')!='')
			begin
			if(ISNULL(@QuestionId,0)>0)
			begin
				update Question set QueDetail = @QuestionText,
								VideoID = @VideoId 
								where QuestionID = @QuestionId
			end
			else
			begin
				
					insert into Question values(@QuestionText,@VideoId)
					set @QuestionId = SCOPE_IDENTITY()
					update #temp set QuestionId = @QuestionId where selfID = @selfId and ISNULL(RedirectVideoType,'') = ISNULL(@RedirectVideoType,'')

				end
		end
		if(ISNULL(@AnswerText,'')!='')
			begin
		declare @tempQuestionId int = (select QuestionId from #temp where selfId = @parentId)
		if(ISNULL(@AnswerId,0)>0)
		begin
			update Answer set AnsDetail = @AnswerText,
								QuestionID = @tempQuestionId,
								VideoID = @VideoId,
								RedirectVideoText = @RedirectVideoText,
								RedirectVideoType = @RedirectVideoType
								where AnswerID = @AnswerId			
		end
		else
		begin
				insert into Answer values(@AnswerText,@tempQuestionId,@VideoId,@RedirectVideoText,@RedirectVideoType)
				set @AnswerId = SCOPE_IDENTITY()
				update #temp set AnswerId = @AnswerId where selfID = @selfId and ISNULL(RedirectVideoType,'') = ISNULL(@RedirectVideoType,'')
			end
		end
		
		
		set @startCount = @startCount + 1
	end
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select * from #temp
	
	delete @tableTemp
	drop table #temp

	SELECT TOP 1 * FROM VideoCampaign
	WHERE Name = @VideoCampaignName AND UserId = @UserId
	ORDER BY VideoCampaignID DESC
END

-- exec sp_videodetail_save_videohierarchy_capaigndetail