-- =============================================
-- Author:		Parth Patel
-- Create date: 12 September 2017
-- Description:	sp_VideoCampaignList_select_by_user
-- =============================================
ALTER PROCEDURE [dbo].[sp_VideoCampaignList_select_by_user]
	-- Add the parameters for the stored procedure here
	@UserId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		vc.VideoCampaignID,
		Name,
		QuestionDuration,
		CONVERT(VARCHAR(10), CreatedDate, 101) AS CreatedDate,
		v.URL
	FROM VideoCampaign vc WITH(NOLOCK)
	inner join Video v on v.VideoCampaignID = vc.VideoCampaignID
	WHERE UserId = @UserId and v.IsStart = 1
END
