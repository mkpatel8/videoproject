-- =============================================
-- Author:		Parth Patel
-- Create date: 16 Nov 2019
-- Description:	Save Campaign Detail and Video Hierarchy
-- =============================================
ALTER PROCEDURE [dbo].[sp_save_capaigndetail_videohierarchy]
	@videoCampaignId int = -1,
	@VideoCampaignName nvarchar(50) = 'Test 2',
	@UserId int = 1,
	@QuestionDuration int = 5,
	@QueBG varchar(20) = null,
	@QueFont varchar(20) = null,
	@AnsBG varchar(20)= null,
	@AnsFont varchar(20)= null,
	@AnsHoverBG varchar(20)= null,
	@AnsHoverFont varchar(20)= null,
	@SourcesJson NVARCHAR(MAX),
	@ElementsJson NVARCHAR(MAX),
	@PathsJson NVARCHAR(MAX),
	@ComposerData NVARCHAR(MAX)
AS
BEGIN

	DECLARE @CurrentVideoCampaignID INT

	IF(@videoCampaignId > 0)
	BEGIN
		UPDATE VideoCampaign SET 
			[Name] = @VideoCampaignName,
			UserId = @UserId,
			QuestionDuration = @QuestionDuration,
			QueBG = @QueBG,
			QueFont = @QueFont,
			AnsBG = @AnsBG,
			AnsFont = @AnsFont,
			AnsHoverFont = @AnsHoverFont,
			AnsHoverBG = @AnsHoverBG,
			ComposerData = @ComposerData
		WHERE VideoCampaignID = @videoCampaignId

		SET @CurrentVideoCampaignID = @videoCampaignId

		DELETE FROM Answer WHERE QuestionID IN (SELECT QuestionID FROM Question WHERE VideoID IN 
		(SELECT VideoID FROM Video WHERE VideoCampaignID = @CurrentVideoCampaignID))

		DELETE FROM Question WHERE VideoID IN (SELECT VideoID FROM Video WHERE VideoCampaignID = @CurrentVideoCampaignID)

		DELETE FROM Video WHERE VideoCampaignID = @CurrentVideoCampaignID
	END
	ELSE
	BEGIN
		INSERT INTO VideoCampaign VALUES(@VideoCampaignName,@UserId,@QuestionDuration,GETDATE(),@QueBG,@QueFont,@AnsBG,@AnsFont,@AnsHoverFont,@AnsHoverBG,@ComposerData)
		SET @CurrentVideoCampaignID = SCOPE_IDENTITY()
	END

	DECLARE @tblSources TABLE
	(
		ID VARCHAR(MAX),
		Title VARCHAR(500),
		Thumbnail VARCHAR(MAX),
		[URL] VARCHAR(MAX)
	)

	INSERT INTO @tblSources
	SELECT * FROM OpenJson(@SourcesJson)
	WITH (
		ID VARCHAR(MAX)   '$.ID',
		Title VARCHAR(500)   '$.Title',
		Thumbnail VARCHAR(MAX)   '$.Thumbnail',
		[URL] VARCHAR(MAX)   '$.URL'
	)	

	DECLARE @tblElements TABLE
	(   
		ID VARCHAR(MAX),
		SourceID VARCHAR(MAX),
		IsStart BIT,
		[Left] VARCHAR(100),
		[Top] VARCHAR(100),
		Question VARCHAR(MAX),
		[Name] VARCHAR(500),
		[Type] VARCHAR(50)
	)

	INSERT INTO @tblElements
	SELECT * FROM OPENJSON ( @ElementsJson )  
	WITH (   

		ID VARCHAR(MAX)   '$.ID',
		SourceID VARCHAR(MAX)   '$.SourceID',
		IsStart BIT   '$.IsStart',
		[Left] VARCHAR(100)   '$.Left',
		[Top] VARCHAR(100)   '$.Top',
		Question VARCHAR(MAX)   '$.Question',
		[Name]  VARCHAR(500)   '$.Name',
		[Type]  VARCHAR(500)   '$.Type'
	)

	DECLARE @tblPaths TABLE
	(
		[From] VARCHAR(MAX),
		[To] VARCHAR(MAX),
		Answer VARCHAR(MAX),

		FromQuestionID INT,
		ToVideoID INT
	)

	INSERT INTO @tblPaths
	SELECT [From], [To], Answer, 0, 0 FROM OpenJson(@PathsJson)
	WITH (
		[From] VARCHAR(MAX)   '$.From',
		[To] VARCHAR(MAX)   '$.To',
		Answer VARCHAR(MAX)   '$.Answer'
	)

	WHILE((SELECT COUNT(1) FROM @tblElements WHERE [Type] = 'V') > 0)
	BEGIN

		DECLARE @CurrentVideoID INT, @CurrentQueID INT

		DECLARE @ElementID VARCHAR(MAX),
		@URL VARCHAR(MAX),
		@IsStart BIT,
		@Left VARCHAR(100),
		@Top VARCHAR(100),
		@Question VARCHAR(MAX),
		@Name VARCHAR(500)

		SELECT TOP 1 @ElementID = ELE.ID, @URL = SRC.[URL], @IsStart = ELE.IsStart,
		@Left = ELE.[Left], @Top = ELE.[Top], @Question = ELE.Question, @Name = ELE.[Name]
		FROM @tblElements AS ELE
		INNER JOIN @tblSources AS SRC ON SRC.ID = ELE.SourceID

		INSERT INTO Video([URL], VideoCampaignID, IsStart)
		VALUES (@URL, @CurrentVideoCampaignID, @IsStart)

		SET @CurrentVideoID = SCOPE_IDENTITY()

		UPDATE @tblPaths SET ToVideoID = @CurrentVideoID WHERE [To] = @ElementID

		IF EXISTS (SELECT 1 FROM @tblPaths WHERE [From] = @ElementID)
		BEGIN
			INSERT INTO Question(QueDetail, VideoID)
			VALUES(@Question, @CurrentVideoID)

			SET @CurrentQueID = SCOPE_IDENTITY()

			UPDATE @tblPaths SET FromQuestionID = @CurrentQueID WHERE [From] = @ElementID
		END

		DELETE FROM @tblElements WHERE ID = @ElementID
	END

	WHILE((SELECT COUNT(1) FROM @tblPaths) > 0)
	BEGIN
		DECLARE @FromID VARCHAR(MAX),
		@ToID VARCHAR(MAX),
		@Answer VARCHAR(MAX),

		@FromQuestionID INT,
		@ToVideoID INT,
		@RedirectVideoText VARCHAR(MAX),
		@RedirectVideoType VARCHAR(50)

		SELECT TOP 1 @FromID = PTH.[From], @ToID = PTH.[To],
		@Answer = PTH.Answer, @FromQuestionID = PTH.FromQuestionID, 
		@ToVideoID = 
			CASE
				WHEN PTH.ToVideoID > 0 THEN PTH.ToVideoID
				ELSE NULL
			END,
		@RedirectVideoText = ELE.Question,
		@RedirectVideoType = 
			CASE
				WHEN ELE.[Type] = 'S' THEN 'Skype'
				WHEN ELE.[Type] = 'L' THEN 'Link'
				WHEN ELE.[Type] = 'P' THEN 'Contact'
				ELSE NULL
			END
		FROM @tblPaths AS PTH
		LEFT JOIN @tblElements AS ELE ON ELE.ID = PTH.[To]

		INSERT INTO Answer(AnsDetail, QuestionID, VideoID, RedirectVideoText, RedirectVideoType)
		VALUES(@Answer, @FromQuestionID, @ToVideoID, @RedirectVideoText, @RedirectVideoType)

		DELETE FROM @tblPaths WHERE [From] = @FromID AND [To] = @ToID
	END

	SELECT @CurrentVideoCampaignID AS VideoCampaignID
END