-- =============================================
-- Author:		Parth Patel
-- Create date: 3 Oct 2017
-- Description:	Insert User Detail
-- =============================================
CREATE PROCEDURE sp_userdetail_insert
	-- Add the parameters for the stored procedure here
	@UserName varchar(50)
	,@UserPassword varchar(50)
	,@UserEmail varchar(50)
	,@UserRole varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @RoleId INT

	SELECT @RoleId=RoleId FROM UserRole WHERE RoleName=@UserRole

	INSERT INTO UserDetail(UserName,UserPassword,UserEmail,RoleId)
	VALUES(@UserName,@UserPassword,@UserEmail,@RoleId)
END
GO
