USE [VideoSoftware]
GO

/****** Object:  UserDefinedTableType [dbo].[SetCampaignDetail]    Script Date: 8/13/2017 10:21:47 PM ******/
CREATE TYPE [dbo].[SetCampaignDetail] AS TABLE(
	[selfID] [int] NULL,
	[parentID] [int] NULL,
	[QuestionText] [nvarchar](max) NULL,
	[AnswerText] [nvarchar](max) NULL,
	[VideoUrl] [varchar](max) NULL,
	[VideoId] [int] NULL,
	[QuestionId] [int] NULL,
	[AnswerId] [int] NULL,
	[RedirectVideoText] [varchar](max) NULL,
	[RedirectVideoType] [varchar](50) NULL
)
GO


