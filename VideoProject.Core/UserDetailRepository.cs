﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoProject.Core.Connection;
using VideoProject.Core.sp;
using VideoProject.Model;

namespace VideoProject.Core
{
    public class UserDetailRepository
    {
        public UserDetailModel VerifyLogin(string UserEmail, string UserPassword)
        {
            UserDetailModel udm = null;

            try
            {
                SqlParameter[] p = new SqlParameter[2];

                p[0] = new SqlParameter("@UserEmail", UserEmail);
                p[1] = new SqlParameter("@UserPassword", UserPassword);

                DataSet ds = MyConnection.Select(SPUserDetail.SELECT_BY_USERNAME_PASSWORD, p);

                if (ds.Tables[0].Rows.Count == 1)
                {

                    DataRow dr = ds.Tables[0].Rows[0];

                    if (UserEmail == Convert.ToString(dr["UserEmail"])
                        && UserPassword == Convert.ToString(dr["UserPassword"])
                        )
                    {
                        udm = new UserDetailModel();
                        udm.UserEmail = UserEmail;
                        udm.UserPassword = UserPassword;
                        udm.UserId = Convert.ToInt32(dr["UserId"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogInsert(ex);
                udm = null;
            }

            return udm;
        }

        public bool Register(string UserName, string UserPassword, string UserEmail)
        {
            bool flag = false;

            try
            {
                SqlParameter[] p = new SqlParameter[4];

                p[0] = new SqlParameter("@UserName", UserName);
                p[1] = new SqlParameter("@UserPassword", UserPassword);
                p[2] = new SqlParameter("@UserEmail", UserEmail);
                p[3] = new SqlParameter("@UserRole", SystemEnum.UserRole.User.ToString());

                MyConnection.Insert(SPUserDetail.INSERT, p);

                flag = true;
            }
            catch (Exception)
            {
                flag = false;
            }

            return flag;
        }

        public UserDetailModel GetUserDetailByEmail(string UserEmail)
        {
            UserDetailModel udm = null;

            try
            {
                SqlParameter[] p = new SqlParameter[1];

                p[0] = new SqlParameter("@UserEmail", UserEmail);

                DataSet ds = MyConnection.Select(SPUserDetail.SELECT_BY_EmailID, p);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];

                    if (UserEmail == Convert.ToString(dr["UserEmail"]))
                    {
                        udm = new UserDetailModel();
                        udm.UserEmail = Convert.ToString(dr["UserEmail"]);
                        udm.RoleId = Convert.ToInt32(dr["RoleId"]);
                        udm.RoleName = Convert.ToString(dr["RoleName"]);
                        udm.Name = Convert.ToString(dr["UserName"]);
                        udm.UserPassword = Convert.ToString(dr["UserPassword"]);
                        udm.UserId = Convert.ToInt32(dr["UserId"]);
                    }
                }
            }
            catch (Exception)
            {
                udm = null;
            }

            return udm;
        }
    }
}