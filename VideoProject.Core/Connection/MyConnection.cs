﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace VideoProject.Core.Connection
{
    public class MyConnection
    {
        private static string MyConStr = ConfigurationManager.ConnectionStrings["VideoSoftwareDBContext"].ConnectionString;
        public MyConnection()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void Insert(String spname, SqlParameter[] p)
        {
            SqlConnection con = new SqlConnection(MyConStr);

            con.Open();

            SqlCommand cmd = new SqlCommand(spname, con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddRange(p);
            cmd.ExecuteNonQuery();

            cmd.Dispose();
            con.Close();
        }

        public static void Update(string spname, SqlParameter[] p)
        {
            SqlConnection con = new SqlConnection(MyConStr);

            con.Open();

            SqlCommand cmd = new SqlCommand(spname, con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddRange(p);
            cmd.ExecuteNonQuery();

            cmd.Dispose();
            con.Close();
        }

        public static void Delete(string spname, SqlParameter[] p)
        {
            SqlConnection con = new SqlConnection(MyConStr);

            con.Open();

            SqlCommand cmd = new SqlCommand(spname, con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddRange(p);
            cmd.ExecuteNonQuery();

            cmd.Dispose();
            con.Close();
        }

        public static void Delete(string spname)
        {
            SqlConnection con = new SqlConnection(MyConStr);

            con.Open();

            SqlCommand cmd = new SqlCommand(spname, con);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.ExecuteNonQuery();

            cmd.Dispose();
            con.Close();
        }

        public static DataSet Select(string spname)
        {
            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection(MyConStr);

            con.Open();

            SqlCommand cmd = new SqlCommand(spname, con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            ad.Fill(ds);

            cmd.Dispose();
            con.Close();

            return ds;
        }

        public static DataSet Select(string spname, SqlParameter[] p)
        {
            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection(MyConStr);

            con.Open();

            SqlCommand cmd = new SqlCommand(spname, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddRange(p);

            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(ds);

            cmd.Dispose();
            con.Close();

            return ds;
        }
    }
}
