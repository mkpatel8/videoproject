﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoProject.Core.Connection;
using VideoProject.Core.sp;
using VideoProject.Model;

namespace VideoProject.Core
{
    public class ButtonSetRepository
    {
        public bool SaveButtonSetDetail(ButtonSetModel bsm)
        {
            bool flag = false;

            try
            {
                SqlParameter[] p = new SqlParameter[1];
                p[0] = new SqlParameter("@xml_ButtonSetModel", SqlDbType.Xml);
                p[0].Value = CommonFunctions.ToXML(bsm);

                MyConnection.Insert(SPButtonSetDetail.BUTTONSET__SAVE, p);

                flag = true;
            }
            catch (Exception ex)
            {
                ErrorLog.ErrorLogInsert(ex);
            }

            return flag;
        }

        public ButtonSetModel GetButtonSetDetail(int ButtonSetID)
        {
            ButtonSetModel bsm = null;

            try
            {
                bsm = new ButtonSetModel();
                bsm.lstButtons = new List<ButtonModel>();

                SqlParameter[] p = new SqlParameter[1];

                p[0] = new SqlParameter("@ButtonSetID", ButtonSetID);

                DataSet ds = MyConnection.Select(SPButtonSetDetail.BUTTONSET_DETAIL_SELECT_BY_BUTTONSETID, p);

                DataRow drButtonSet = ds.Tables[0].Rows[0];

                bsm.ButtonSetName = Convert.ToString(drButtonSet["ButtonSetName"]);
                bsm.IsDisplayHorizontal = Convert.ToBoolean(drButtonSet["IsDisplayHorizontal"]);

                DataTable dtButton = ds.Tables[1];

                foreach (DataRow drButton in dtButton.Rows)
                {
                    ButtonModel bm = new ButtonModel();

                    bm.ButtonText = Convert.ToString(drButton["ButtonText"]);
                    bm.ButtonBackgroundColor = Convert.ToString(drButton["ButtonBackgroundColor"]);
                    bm.ButtonFontColor = Convert.ToString(drButton["ButtonFontColor"]);
                    bm.ButtonHoverBackgroundColor = Convert.ToString(drButton["ButtonHoverBackgroundColor"]);
                    bm.ButtonHoverFontColor = Convert.ToString(drButton["ButtonHoverFontColor"]);
                    bm.ButtonLink = Convert.ToString(drButton["ButtonLink"]);

                    bsm.lstButtons.Add(bm);
                }
            }
            catch (Exception ex)
            {
                bsm = null;
                ErrorLog.ErrorLogInsert(ex);
            }

            return bsm;
        }

        public List<ButtonSetModel> GetButtonSetList(int UserID)
        {
            List<ButtonSetModel> lstButtonSet;

            try
            {
                lstButtonSet = new List<ButtonSetModel>();

                SqlParameter[] p = new SqlParameter[1];

                p[0] = new SqlParameter("@UserID", UserID);

                DataSet ds = MyConnection.Select(SPButtonSetDetail.BUTTONSET_LIST_SELECT_BY_USER, p);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ButtonSetModel bsm = new ButtonSetModel();

                    bsm.ButtonSetID = Convert.ToInt32(dr["ButtonSetID"]);
                    bsm.ButtonSetName = Convert.ToString(dr["ButtonSetName"]);
                    bsm.IsDisplayHorizontal = Convert.ToBoolean(dr["IsDisplayHorizontal"]);
                    
                    lstButtonSet.Add(bsm);
                }
            }
            catch (Exception ex)
            {
                lstButtonSet = null;
                ErrorLog.ErrorLogInsert(ex);
            }

            return lstButtonSet;
        }
    }
}
