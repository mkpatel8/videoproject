﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Core.sp
{
    public class SPUserDetail
    {
        public static string SELECT_BY_USERNAME_PASSWORD = "sp_userdetail_select_by_username_password";
        public static string INSERT = "sp_userdetail_insert";
        public static string SELECT_BY_EmailID = "sp_userdetail_select_by_useremail";

    }
}
