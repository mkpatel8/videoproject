﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Core.sp
{
    public class SPButtonSetDetail
    {
        public static string BUTTONSET__SAVE = "sp_buttonsetdetail_save";
        public static string BUTTONSET_DETAIL_SELECT_BY_BUTTONSETID = "sp_buttonsetdetail_select_by_buttonsetid";
        public static string BUTTONSET_LIST_SELECT_BY_USER = "sp_buttonsetlist_select_by_user";
    }
}
