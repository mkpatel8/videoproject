﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Core.sp
{
    public class SPVideoDetail
    {
        public static string VIDEO_HIERARCHY_SELECT_BY_VIDEOCAMPAIGNID = "sp_videohierarchy_select_by_video_campaign_id";
        public static string VIDEO_CAMPAIGN_SELECT_BY_NAME_USER = "sp_VideoCampaign_select_by_name_user";
        public static string VIDEO_HIERARCHY_CAMPAIGN_DETAIL_SAVE = "sp_videodetail_save_videohierarchy_capaigndetail";
        public static string VIDEO_CAMPAIGN_LIST_SELECT_BY_USER = "sp_VideoCampaignList_select_by_user";
        public static string CAMPAIGN_DETAIL_VIDEO_HIERARCHY_SAVE = "sp_save_capaigndetail_videohierarchy";
        public static string VIDEO_CAMPAIGN_SELECT_BY_VIDEOCAMPAIGNID = "sp_videocampaign_select_by_video_campaign_id";
        public static string VIDEO_SELECT_BY_UNIQUEID = "sp_videomodel_select_by_video_campaign_id";
        public static string VIDEO_ELEMENT_SAVE = "sp_videoelement_save";
        public static string VIDEO_CAMPAIGN_DELETE = "sp_videocampaign_delete_by_video_campaign_id";
    }
}
