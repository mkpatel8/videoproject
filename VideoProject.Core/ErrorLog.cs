﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoProject.Core
{
    public class ErrorLog
    {
        public static string fileName = "ErrorLog.txt";
        public static string errorFolderPath = Path.Combine(Environment.CurrentDirectory, @"Error\");
        public static string errorFilePath = Path.Combine(Environment.CurrentDirectory, @"Error\", fileName);

        public static void ErrorLogInsert(string logDetail)
        {
            if (!Directory.Exists(errorFolderPath))
            {
                Directory.CreateDirectory(errorFolderPath);
            }

            if (!File.Exists(errorFilePath))
            {
                File.Create(errorFilePath).Close();
            }

            using (StreamWriter writer = new StreamWriter(errorFilePath, true))
            {
                writer.WriteLine(logDetail);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }

        public static void ErrorLogInsert(Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Message :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());

            ErrorLogInsert(sb.ToString());

            //sb.Append(Environment.NewLine);
            //sb.Append("--------------------------------------");

            //sb.Append(Environment.NewLine);
            //sb.Append("Error Message : " + ex.Message);
            //sb.Append(Environment.NewLine);
            //sb.Append("Source : " + ex.Source);
            //sb.Append(Environment.NewLine);
            //sb.Append("StackTrace : " + ex.StackTrace);
            //sb.Append(Environment.NewLine);
            //sb.Append("Date : " + DateTime.Now);
            //sb.Append(Environment.NewLine);

            //sb.Append("--------------------------------------");
            //sb.Append(Environment.NewLine);
        }
    }
}
